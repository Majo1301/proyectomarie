﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmInventario : Form
    {
        ProductosManejador _productosManejador;
        InventarioManejador _inventarioManejador;
        public frmInventario()
        {
            InitializeComponent();
            _productosManejador = new ProductosManejador();
            _inventarioManejador = new InventarioManejador();
        }
        public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            gbtexto.Visible = c;
        }
        private void buscarInventario(string filtro)
        {
            Dtg_precios.DataSource = _inventarioManejador.GetInventario(filtro);
        }
        private void Modificar()
        {
            lblStatus.Text = "1";
            controlbotones(false, false, true);
            txtId.Text = Dtg_precios.CurrentRow.Cells["id"].Value.ToString();
            txtNombre.Text = Dtg_precios.CurrentRow.Cells["nombre"].Value.ToString();
            txtCantidad.Text = Dtg_precios.CurrentRow.Cells["cantidad"].Value.ToString();
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            controlbotones(false, false, true);
        }
        private void guardar()
        {
            _inventarioManejador.Guardar(new Inventario
            {
                Id = int.Parse(txtId.Text),
                Nombre = txtNombre.Text,
                Cantidad = Convert.ToInt32(txtCantidad.Value),
            }, int.Parse(lblStatus.Text)); ;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlbotones(true, false, false);
            try
            {
                    guardar();
                    buscarInventario("");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }

        private void Dtg_precios_DoubleClick(object sender, EventArgs e)
        {
           /* try
            {
                lblStatus.Text = "1";
                Modificar();
                buscarInventario("");
                Dtg_precios.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true, false, false);
        }

        private void frmInventario_Load(object sender, EventArgs e)
        {
            buscarInventario("");
        }
        private void obtenerProducto(string filtro)
        {
            txtNombre.DataSource = _productosManejador.GetProductos(filtro);
            txtNombre.DisplayMember = "nombre";
            txtNombre.ValueMember = "nombre";
        }

        private void txtNombre_Click(object sender, EventArgs e)
        {
            obtenerProducto("");
        }
    }
}
