﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class GastosManejador
    {
        private GastosAD _gastosAD = new GastosAD();
        public void Guardar(Gastos Gasto, int lbl)
        {
            _gastosAD.Guardar(Gasto, lbl);
        }
        public void Eliminar(int id)
        {
            _gastosAD.Eliminar(id);
        }
        public List<Gastos> GetGastos(string filtro)
        {
            var listProductos = _gastosAD.GetGastos(filtro);
            return listProductos;
        }
    }
}
