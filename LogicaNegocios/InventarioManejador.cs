﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class InventarioManejador
    {
        private InventarioAD _inventarioAD = new InventarioAD();
        public void Guardar(Inventario inventario, int lbl)
        {
            _inventarioAD.Guardar(inventario, lbl);
        }
        public List<Inventario> GetInventario(string filtro)
        {
            var listInventario = _inventarioAD.GetInventario(filtro);
            return listInventario;
        }
        public void Compra(string nombre, int cantidad)
        {
            _inventarioAD.Compra(nombre,cantidad);
        }
        public void CompraEliminar(string nombre, int cantidad)
        {
            _inventarioAD.CompraEliminar(nombre, cantidad);
        }
    }
}
