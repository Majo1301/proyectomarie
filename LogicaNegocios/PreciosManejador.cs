﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public  class PreciosManejador
    {
        private PrecioAD _precioAD = new PrecioAD();
        public List<Precios> GetPrecios(string filtro)
        {
            var listPrecios = _precioAD.GetPrecio(filtro);
            return listPrecios;
        }
        public void Guardar(Precios precios, int lbl, int menu)
        {
            _precioAD.Guardar(precios, lbl, menu);
        }
        public List<Precios> GetPrecioVenta(string filtro)
        {
            var listPrecios = _precioAD.GetPrecioVenta(filtro);
            return listPrecios;
        }
        public string GetPrecioVentaA(string filtro)
        {
            var variable = _precioAD.GetPrecioVentaA(filtro);
            return variable;
        }
        public string GetPrecioVentaB(string filtro)
        {
            var variable = _precioAD.GetPrecioVentaB(filtro);
            return variable;
        }
    }
}
