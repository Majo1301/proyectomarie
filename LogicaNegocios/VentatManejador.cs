﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;


namespace LogicaNegocios
{
    public class VentatManejador
    {
        private VentatAD _ventatAD = new VentatAD(); 
        public void Guardar(Ventat ventat, int lbl)
        {
            _ventatAD.Guardar(ventat, lbl);
        }
        public void GuardarEnVenta(int idp, double subtotal, double total, string fecha)
        {
            _ventatAD.GuardarEnVenta(idp, subtotal, total,fecha);
        }
        public void Eliminar(int id)
        {
            _ventatAD.Eliminar(id);
        }
        public List<Ventat> GetVentat(string filtro)
        {
            var listProductos = _ventatAD.GetVentat(filtro);
            return listProductos;
        }
        public string GetUltimaVenta()
        {
            var variable = _ventatAD.GetUltimaVenta();
            return variable;
        }
        public string GetPrecioTotalVenta(string filtro)
        {
            var variable = _ventatAD.GetPrecioTotalVenta(filtro);
            return variable;
        }
        
            
    }
}
