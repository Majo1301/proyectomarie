﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class CorteDiaManejador
    {
        private CorteDiaAD _corteDiaAD = new CorteDiaAD();
        public void Guardar(CorteDia corteDia, int lbl)
        {
            _corteDiaAD.Guardar(corteDia, lbl);
        }
        public void Eliminar(int id)
        {
            _corteDiaAD.Eliminar(id);
        }
        public List<CorteDia> GetCorteDia(string filtro)
        {
            var listCorteDia = _corteDiaAD.GetCorteDia(filtro);
            return listCorteDia;
        }
    }
}
