﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class CorteSemanalManejador
    {
        private CorteSemanalAD _corteSemanalAD = new CorteSemanalAD();
        public void Guardar(CorteSemanal corteSemanal, int lbl)
        {
            _corteSemanalAD.Guardar(corteSemanal, lbl);
        }
        public List<CorteSemanal> GetCorteSemanal(string filtro)
        {
            var listCorteSemanal = _corteSemanalAD.GetCortSemanal(filtro);
            return listCorteSemanal;
        }
    }
}
