﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class VentasManejador
    {
        private VentasAD _ventasAD = new VentasAD();
        public void Guardar(Ventas ventas, int lbl, string movimiento)
        {
            _ventasAD.Guardar(ventas, lbl, movimiento);
        }
        public void Eliminar(int id)
        {
            _ventasAD.Eliminar(id);
        }
        public List<Ventas> GetVentas(string filtro)
        {
            var listProductos = _ventasAD.GetVentas(filtro);
            return listProductos;
        }
    }
}
