﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class ProductosManejador
    {
        private ProductosSD _productosSD = new ProductosSD();
        public void Guardar(Productos productos, int lbl)
        {
            _productosSD.Guardar(productos, lbl);
        }
        public List<Productos> GetProductos(string filtro)
        {
            var listProductos = _productosSD.GetProducto(filtro);
            return listProductos;
        }
    }
}
