﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;


namespace LogicaNegocios
{
    public class RegistroVentasManejador
    {
        private RegistroVentasAD _registroVentasAD = new RegistroVentasAD();

        public void Guardar(RegistroVentas registroVentas, int lbl)
        {
            _registroVentasAD.Guardar(registroVentas, lbl);
        }
        public void Eliminar(int id)
        {
            _registroVentasAD.Eliminar(id);
        }
        public List<RegistroVentas> GetVentas(string filtro)
        {
            var listVentas = _registroVentasAD.GetVentas(filtro);
            return listVentas;
        }
        public List<RegistroVentas> GetFecha(string filtro)
        {
            var listVentas = _registroVentasAD.GetFecha(filtro);
            return listVentas;
        }
        public List<RegistroVentas> GetFechas(string filtro, string filtro2)
        {
            var listVentas = _registroVentasAD.GetFechas(filtro, filtro2);
            return listVentas;
        }
    }
}
