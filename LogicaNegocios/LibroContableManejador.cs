﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;

namespace LogicaNegocios
{
    public class LibroContableManejador
    {
        private LibroContableAD _libroContableAD = new LibroContableAD();
        public void Guardar(int lbl, string fecha)
        {
            _libroContableAD.Guardar(lbl, fecha);
        }
        public List<LibroContable> GetLibroContable(string filtro)
        {
            var listLibroContable = _libroContableAD.GetLibroContable(filtro);
            return listLibroContable;
        }
        public List<LibroContable> GetLibroContableFecha(string filtro)
        {
            var listLibroContable = _libroContableAD.GetLibroContableFecha(filtro);
            return listLibroContable;
        }
    }
}
