﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MarieCosmetics2._0
{
    public partial class frmCategoria : Form
    {
        public frmCategoria()
        {
            InitializeComponent();
            EstadoInicial();
            datosDTGcategoria();
        }
        //SqlConnection conexion = new SqlConnection("data source=DESKTOPF2F; initial catalog=Marie_DB; user id=sa; password=123456789");
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        public void EstadoInicial()
        {
            lblStatus.Text = "0";
            Txt_Id.Text = "";
            Txt_categoria.Text = "";
            Txt_descripcion.Text = "";
        }
        public void datosDTGcategoria()
        {
            conexion.Open();
            var adaptador = new SqlDataAdapter("select * from categoria", conexion);
            var command = new SqlCommandBuilder(adaptador);
            var ds = new DataSet();
            adaptador.Fill(ds);
            DtgListaproveedor.ReadOnly = true;
            DtgListaproveedor.DataSource = ds.Tables[0];
            conexion.Close();
        }
        public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }
        private void frmCategoria_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblStatus.Text=="0")
            {
                try
                {
                    string consulta = "INSERT INTO categoria VALUES('" + Txt_categoria.Text + "','" + Txt_descripcion.Text + "')";
                    SqlCommand cmd = new SqlCommand(consulta, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Categoría Guardado Correctamente.");
                    EstadoInicial();
                    datosDTGcategoria();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                    controlbotones(true, true, false);
                }
            }
            else 
            {
                try
                {
                    string update = "UPDATE categoria set tipo_categoria = '" + Txt_categoria.Text + "', descipcion = '" + Txt_descripcion.Text +  "' where id_categoria = '" + double.Parse(Txt_Id.Text) + "'";
                    SqlCommand cmd = new SqlCommand(update, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Categoria editado Correctamente.");
                    EstadoInicial();
                    datosDTGcategoria();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                    controlbotones(true, true, false);
                }
            }
        }

        private void DtgListaproveedor_DoubleClick(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            EstadoInicial();
            controlbotones(false, false, true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true, true, false);
        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            EstadoInicial();
            controlbotones(false, false, true);
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(DtgListaproveedor.CurrentRow.Cells["id_categoria"].Value);
            try
            {
                string consulta = "delete from categoria where id_categoria = '" + id.ToString() + "'";
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                conexion.Open();
                cmd.ExecuteNonQuery();
                conexion.Close();
                MessageBox.Show("Categoria Eliminada Correctamente.");
                EstadoInicial();
                datosDTGcategoria();
                controlbotones(true, true, false);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
            controlbotones(true, true, false);
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            controlbotones(true, true, false);
        }

        private void DtgListaproveedor_DoubleClick_1(object sender, EventArgs e)
        {
            lblStatus.Text = "1";
            try
            {
                Txt_Id.Text = DtgListaproveedor.CurrentRow.Cells["id_categoria"].Value.ToString();
                Txt_categoria.Text = DtgListaproveedor.CurrentRow.Cells["tipo_categoria"].Value.ToString();
                Txt_descripcion.Text = DtgListaproveedor.CurrentRow.Cells["descipcion"].Value.ToString();
                controlbotones(false, false, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                controlbotones(true, true, false);
            }
        }
    }
}
