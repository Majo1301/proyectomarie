﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmCorteSemanal : Form
    {
        CorteSemanalManejador _corteSemanalManejador;
        LibroContableManejador _libroContableManejador;
        public frmCorteSemanal()
        {
            InitializeComponent();
            _corteSemanalManejador = new CorteSemanalManejador();
            _libroContableManejador = new LibroContableManejador();
        }

        private void buscarCorteSemanal(string filtro)
        {
            dgvDatos.DataSource = _corteSemanalManejador.GetCorteSemanal(filtro);
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                guardar();
                guardarLibro();
                //limpiarcuadros();
                buscarCorteSemanal("");
                //gbtexto.Visible = false;
                dgvDatos.Visible = true;
                dgvDatos.Enabled = true;
                // btnAgregar.Visible = true;
                //  btnEliminar.Visible = true;
                //txtId.Enabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }
        private void guardar()
        {
            try
            {
                _corteSemanalManejador.Guardar(new CorteSemanal
                {
                    Id = int.Parse("0"),
                    Fecha = dtpFecha.Text,
                    Fechas = dtpFecha.Text,
                    Total = Convert.ToDouble(0.0),
                }, int.Parse(lblStatus.Text)); ;
            }
            catch (Exception ex)
            {
            }
        }
        private void guardarLibro()
        {
            try
            {
                _libroContableManejador.Guardar(int.Parse(lblStatus.Text), dtpFecha.Text); ;
            }
            catch (Exception ex)
            {
            }
        }

        private void frmCorteSemanal_Load(object sender, EventArgs e)
        {
            try
            {
                buscarCorteSemanal("");
            }
            catch (Exception ex)
            {
                MessageBox.Show("no se pudo " + ex);
            }
            //controlarbotones(false, false, false, false);
            //controlarcuadros(false);
            //limpiarcuadros();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {

        }
    }
}
