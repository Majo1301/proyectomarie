﻿
namespace MarieCosmetics2._0
{
    partial class frmMenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenuPrincipal));
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.lblTipo = new System.Windows.Forms.Label();
            this.lblCerrarS = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.BarraVertical = new System.Windows.Forms.Panel();
            this.btnInventario = new System.Windows.Forms.Button();
            this.btnCategoria = new System.Windows.Forms.Button();
            this.btnProveedores = new System.Windows.Forms.Button();
            this.btnLibroContable = new System.Windows.Forms.Button();
            this.btnPrecioProductos = new System.Windows.Forms.Button();
            this.btnRegistroES = new System.Windows.Forms.Button();
            this.pContenido = new System.Windows.Forms.Panel();
            this.btnRegistroProducto = new System.Windows.Forms.Button();
            this.btnCorteSemana = new System.Windows.Forms.Button();
            this.btnCorteDia = new System.Windows.Forms.Button();
            this.btnAdministrarUsuarios = new System.Windows.Forms.Button();
            this.btnRegistroVentas = new System.Windows.Forms.Button();
            this.btnAgregarVenta = new System.Windows.Forms.PictureBox();
            this.btnVentas = new System.Windows.Forms.Button();
            this.btnControlUsuarios = new System.Windows.Forms.Button();
            this.btnFinanzas = new System.Windows.Forms.Button();
            this.btnControlGastos = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.pContenidoo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BarraTitulo.SuspendLayout();
            this.BarraVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAgregarVenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.BarraTitulo.Controls.Add(this.lblTipo);
            this.BarraTitulo.Controls.Add(this.lblCerrarS);
            this.BarraTitulo.Controls.Add(this.lblUsuario);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.ForeColor = System.Drawing.Color.Black;
            this.BarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1370, 35);
            this.BarraTitulo.TabIndex = 0;
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.ForeColor = System.Drawing.Color.White;
            this.lblTipo.Location = new System.Drawing.Point(4, 7);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(19, 21);
            this.lblTipo.TabIndex = 2;
            this.lblTipo.Text = "1";
            this.lblTipo.Visible = false;
            // 
            // lblCerrarS
            // 
            this.lblCerrarS.AutoSize = true;
            this.lblCerrarS.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrarS.ForeColor = System.Drawing.Color.White;
            this.lblCerrarS.Location = new System.Drawing.Point(1249, 7);
            this.lblCerrarS.Name = "lblCerrarS";
            this.lblCerrarS.Size = new System.Drawing.Size(104, 21);
            this.lblCerrarS.TabIndex = 1;
            this.lblCerrarS.Text = "Cerrar Sesion";
            this.lblCerrarS.Click += new System.EventHandler(this.lblCerrarS_Click);
            this.lblCerrarS.MouseLeave += new System.EventHandler(this.lblCerrarS_MouseLeave);
            this.lblCerrarS.MouseHover += new System.EventHandler(this.lblCerrarS_MouseHover);
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.Indigo;
            this.lblUsuario.Location = new System.Drawing.Point(29, 7);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(64, 21);
            this.lblUsuario.TabIndex = 0;
            this.lblUsuario.Text = "Usuario";
            // 
            // BarraVertical
            // 
            this.BarraVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.BarraVertical.Controls.Add(this.btnInventario);
            this.BarraVertical.Controls.Add(this.btnCategoria);
            this.BarraVertical.Controls.Add(this.btnProveedores);
            this.BarraVertical.Controls.Add(this.btnLibroContable);
            this.BarraVertical.Controls.Add(this.btnPrecioProductos);
            this.BarraVertical.Controls.Add(this.btnRegistroES);
            this.BarraVertical.Controls.Add(this.pContenido);
            this.BarraVertical.Controls.Add(this.btnRegistroProducto);
            this.BarraVertical.Controls.Add(this.btnCorteSemana);
            this.BarraVertical.Controls.Add(this.btnCorteDia);
            this.BarraVertical.Controls.Add(this.btnAdministrarUsuarios);
            this.BarraVertical.Controls.Add(this.btnRegistroVentas);
            this.BarraVertical.Controls.Add(this.btnAgregarVenta);
            this.BarraVertical.Controls.Add(this.btnVentas);
            this.BarraVertical.Controls.Add(this.btnControlUsuarios);
            this.BarraVertical.Controls.Add(this.btnFinanzas);
            this.BarraVertical.Controls.Add(this.btnControlGastos);
            this.BarraVertical.Controls.Add(this.btnProductos);
            this.BarraVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.BarraVertical.Location = new System.Drawing.Point(0, 35);
            this.BarraVertical.Name = "BarraVertical";
            this.BarraVertical.Size = new System.Drawing.Size(256, 753);
            this.BarraVertical.TabIndex = 1;
            // 
            // btnInventario
            // 
            this.btnInventario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnInventario.FlatAppearance.BorderSize = 0;
            this.btnInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInventario.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInventario.ForeColor = System.Drawing.Color.White;
            this.btnInventario.Location = new System.Drawing.Point(0, 437);
            this.btnInventario.Name = "btnInventario";
            this.btnInventario.Size = new System.Drawing.Size(256, 35);
            this.btnInventario.TabIndex = 10;
            this.btnInventario.Text = "Inventario";
            this.btnInventario.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnInventario.UseVisualStyleBackColor = false;
            this.btnInventario.Click += new System.EventHandler(this.btnInventario_Click);
            this.btnInventario.MouseLeave += new System.EventHandler(this.btnInventario_MouseLeave);
            this.btnInventario.MouseHover += new System.EventHandler(this.btnInventario_MouseHover);
            // 
            // btnCategoria
            // 
            this.btnCategoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnCategoria.FlatAppearance.BorderSize = 0;
            this.btnCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategoria.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategoria.ForeColor = System.Drawing.Color.White;
            this.btnCategoria.Location = new System.Drawing.Point(0, 401);
            this.btnCategoria.Name = "btnCategoria";
            this.btnCategoria.Size = new System.Drawing.Size(256, 35);
            this.btnCategoria.TabIndex = 9;
            this.btnCategoria.Text = "Categoria";
            this.btnCategoria.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCategoria.UseVisualStyleBackColor = false;
            this.btnCategoria.Click += new System.EventHandler(this.btnCategoria_Click);
            this.btnCategoria.MouseLeave += new System.EventHandler(this.btnCategoria_MouseLeave);
            this.btnCategoria.MouseHover += new System.EventHandler(this.btnCategoria_MouseHover);
            // 
            // btnProveedores
            // 
            this.btnProveedores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnProveedores.FlatAppearance.BorderSize = 0;
            this.btnProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProveedores.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProveedores.ForeColor = System.Drawing.Color.White;
            this.btnProveedores.Location = new System.Drawing.Point(0, 365);
            this.btnProveedores.Name = "btnProveedores";
            this.btnProveedores.Size = new System.Drawing.Size(256, 35);
            this.btnProveedores.TabIndex = 8;
            this.btnProveedores.Text = "Proveedores";
            this.btnProveedores.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnProveedores.UseVisualStyleBackColor = false;
            this.btnProveedores.Click += new System.EventHandler(this.btnProveedores_Click);
            this.btnProveedores.MouseLeave += new System.EventHandler(this.btnProveedores_MouseLeave);
            this.btnProveedores.MouseHover += new System.EventHandler(this.btnProveedores_MouseHover);
            // 
            // btnLibroContable
            // 
            this.btnLibroContable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnLibroContable.FlatAppearance.BorderSize = 0;
            this.btnLibroContable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLibroContable.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLibroContable.ForeColor = System.Drawing.Color.White;
            this.btnLibroContable.Location = new System.Drawing.Point(0, 525);
            this.btnLibroContable.Name = "btnLibroContable";
            this.btnLibroContable.Size = new System.Drawing.Size(256, 35);
            this.btnLibroContable.TabIndex = 7;
            this.btnLibroContable.Text = "Libro Contable";
            this.btnLibroContable.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnLibroContable.UseVisualStyleBackColor = false;
            this.btnLibroContable.Click += new System.EventHandler(this.btnLibroContable_Click);
            this.btnLibroContable.MouseLeave += new System.EventHandler(this.btnLibroContable_MouseLeave);
            this.btnLibroContable.MouseHover += new System.EventHandler(this.btnLibroContable_MouseHover);
            // 
            // btnPrecioProductos
            // 
            this.btnPrecioProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnPrecioProductos.FlatAppearance.BorderSize = 0;
            this.btnPrecioProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrecioProductos.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrecioProductos.ForeColor = System.Drawing.Color.White;
            this.btnPrecioProductos.Location = new System.Drawing.Point(0, 329);
            this.btnPrecioProductos.Name = "btnPrecioProductos";
            this.btnPrecioProductos.Size = new System.Drawing.Size(256, 35);
            this.btnPrecioProductos.TabIndex = 6;
            this.btnPrecioProductos.Text = "Compra/Precio Productos";
            this.btnPrecioProductos.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnPrecioProductos.UseVisualStyleBackColor = false;
            this.btnPrecioProductos.Click += new System.EventHandler(this.btnPrecioProductos_Click);
            this.btnPrecioProductos.MouseLeave += new System.EventHandler(this.btnPrecioProductos_MouseLeave);
            this.btnPrecioProductos.MouseHover += new System.EventHandler(this.btnPrecioProductos_MouseHover);
            // 
            // btnRegistroES
            // 
            this.btnRegistroES.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnRegistroES.FlatAppearance.BorderSize = 0;
            this.btnRegistroES.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistroES.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistroES.ForeColor = System.Drawing.Color.White;
            this.btnRegistroES.Location = new System.Drawing.Point(1, 205);
            this.btnRegistroES.Name = "btnRegistroES";
            this.btnRegistroES.Size = new System.Drawing.Size(255, 35);
            this.btnRegistroES.TabIndex = 4;
            this.btnRegistroES.Text = "Registro Entrada/Salida";
            this.btnRegistroES.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistroES.UseVisualStyleBackColor = false;
            this.btnRegistroES.Click += new System.EventHandler(this.btnRegistroES_Click);
            this.btnRegistroES.MouseLeave += new System.EventHandler(this.btnRegistroES_MouseLeave);
            this.btnRegistroES.MouseHover += new System.EventHandler(this.btnRegistroES_MouseHover);
            // 
            // pContenido
            // 
            this.pContenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(111)))), ((int)(((byte)(222)))));
            this.pContenido.Location = new System.Drawing.Point(368, 410);
            this.pContenido.Name = "pContenido";
            this.pContenido.Size = new System.Drawing.Size(260, 691);
            this.pContenido.TabIndex = 3;
            // 
            // btnRegistroProducto
            // 
            this.btnRegistroProducto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnRegistroProducto.FlatAppearance.BorderSize = 0;
            this.btnRegistroProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistroProducto.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistroProducto.ForeColor = System.Drawing.Color.White;
            this.btnRegistroProducto.Location = new System.Drawing.Point(0, 293);
            this.btnRegistroProducto.Name = "btnRegistroProducto";
            this.btnRegistroProducto.Size = new System.Drawing.Size(256, 35);
            this.btnRegistroProducto.TabIndex = 4;
            this.btnRegistroProducto.Text = "Agregar Productos";
            this.btnRegistroProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistroProducto.UseVisualStyleBackColor = false;
            this.btnRegistroProducto.Click += new System.EventHandler(this.btnRegistroProducto_Click);
            this.btnRegistroProducto.MouseLeave += new System.EventHandler(this.btnRegistroProducto_MouseLeave);
            this.btnRegistroProducto.MouseHover += new System.EventHandler(this.btnRegistroProducto_MouseHover);
            // 
            // btnCorteSemana
            // 
            this.btnCorteSemana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnCorteSemana.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnCorteSemana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCorteSemana.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCorteSemana.ForeColor = System.Drawing.Color.White;
            this.btnCorteSemana.Location = new System.Drawing.Point(1, 170);
            this.btnCorteSemana.Name = "btnCorteSemana";
            this.btnCorteSemana.Size = new System.Drawing.Size(255, 35);
            this.btnCorteSemana.TabIndex = 5;
            this.btnCorteSemana.Text = "Semanal";
            this.btnCorteSemana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorteSemana.UseVisualStyleBackColor = false;
            this.btnCorteSemana.Click += new System.EventHandler(this.btnCorteSemana_Click);
            this.btnCorteSemana.MouseLeave += new System.EventHandler(this.btnCorteSemana_MouseLeave);
            this.btnCorteSemana.MouseHover += new System.EventHandler(this.btnCorteSemana_MouseHover);
            // 
            // btnCorteDia
            // 
            this.btnCorteDia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnCorteDia.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnCorteDia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCorteDia.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCorteDia.ForeColor = System.Drawing.Color.White;
            this.btnCorteDia.Location = new System.Drawing.Point(1, 136);
            this.btnCorteDia.Name = "btnCorteDia";
            this.btnCorteDia.Size = new System.Drawing.Size(255, 35);
            this.btnCorteDia.TabIndex = 4;
            this.btnCorteDia.Text = "Dia";
            this.btnCorteDia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorteDia.UseVisualStyleBackColor = false;
            this.btnCorteDia.Click += new System.EventHandler(this.btnCorteDia_Click);
            this.btnCorteDia.MouseLeave += new System.EventHandler(this.btnCorteDia_MouseLeave);
            this.btnCorteDia.MouseHover += new System.EventHandler(this.btnCorteDia_MouseHover);
            // 
            // btnAdministrarUsuarios
            // 
            this.btnAdministrarUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnAdministrarUsuarios.FlatAppearance.BorderSize = 0;
            this.btnAdministrarUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdministrarUsuarios.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdministrarUsuarios.ForeColor = System.Drawing.Color.White;
            this.btnAdministrarUsuarios.Location = new System.Drawing.Point(0, 612);
            this.btnAdministrarUsuarios.Name = "btnAdministrarUsuarios";
            this.btnAdministrarUsuarios.Size = new System.Drawing.Size(256, 35);
            this.btnAdministrarUsuarios.TabIndex = 4;
            this.btnAdministrarUsuarios.Text = "Administrar Usuarios";
            this.btnAdministrarUsuarios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdministrarUsuarios.UseVisualStyleBackColor = false;
            this.btnAdministrarUsuarios.Click += new System.EventHandler(this.btnAdministrarUsuarios_Click);
            this.btnAdministrarUsuarios.MouseLeave += new System.EventHandler(this.btnAdministrarUsuarios_MouseLeave);
            this.btnAdministrarUsuarios.MouseHover += new System.EventHandler(this.btnAdministrarUsuarios_MouseHover);
            // 
            // btnRegistroVentas
            // 
            this.btnRegistroVentas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnRegistroVentas.FlatAppearance.BorderSize = 0;
            this.btnRegistroVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistroVentas.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistroVentas.ForeColor = System.Drawing.Color.White;
            this.btnRegistroVentas.Location = new System.Drawing.Point(0, 49);
            this.btnRegistroVentas.Name = "btnRegistroVentas";
            this.btnRegistroVentas.Size = new System.Drawing.Size(256, 35);
            this.btnRegistroVentas.TabIndex = 4;
            this.btnRegistroVentas.Text = "Registro Ventas";
            this.btnRegistroVentas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistroVentas.UseVisualStyleBackColor = false;
            this.btnRegistroVentas.Click += new System.EventHandler(this.btnRegistroVentas_Click);
            this.btnRegistroVentas.MouseLeave += new System.EventHandler(this.btnRegistroVentas_MouseLeave);
            this.btnRegistroVentas.MouseHover += new System.EventHandler(this.btnRegistroVentas_MouseHover);
            // 
            // btnAgregarVenta
            // 
            this.btnAgregarVenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(33)))), ((int)(((byte)(63)))));
            this.btnAgregarVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarVenta.Image")));
            this.btnAgregarVenta.Location = new System.Drawing.Point(13, 11);
            this.btnAgregarVenta.Name = "btnAgregarVenta";
            this.btnAgregarVenta.Size = new System.Drawing.Size(26, 26);
            this.btnAgregarVenta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnAgregarVenta.TabIndex = 3;
            this.btnAgregarVenta.TabStop = false;
            this.btnAgregarVenta.Click += new System.EventHandler(this.btnAgregarVenta_Click);
            this.btnAgregarVenta.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnAgregarVenta_MouseDown);
            this.btnAgregarVenta.MouseLeave += new System.EventHandler(this.btnAgregarVenta_MouseLeave);
            this.btnAgregarVenta.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            this.btnAgregarVenta.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAgregarVenta_MouseUp);
            // 
            // btnVentas
            // 
            this.btnVentas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnVentas.FlatAppearance.BorderSize = 0;
            this.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentas.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVentas.ForeColor = System.Drawing.Color.White;
            this.btnVentas.Location = new System.Drawing.Point(0, 0);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Size = new System.Drawing.Size(256, 46);
            this.btnVentas.TabIndex = 2;
            this.btnVentas.Text = "Ventas";
            this.btnVentas.UseVisualStyleBackColor = false;
            // 
            // btnControlUsuarios
            // 
            this.btnControlUsuarios.AutoSize = true;
            this.btnControlUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnControlUsuarios.FlatAppearance.BorderSize = 0;
            this.btnControlUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnControlUsuarios.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlUsuarios.ForeColor = System.Drawing.Color.White;
            this.btnControlUsuarios.Location = new System.Drawing.Point(0, 563);
            this.btnControlUsuarios.Name = "btnControlUsuarios";
            this.btnControlUsuarios.Size = new System.Drawing.Size(253, 46);
            this.btnControlUsuarios.TabIndex = 3;
            this.btnControlUsuarios.Text = "Control de Usuarios";
            this.btnControlUsuarios.UseVisualStyleBackColor = false;
            // 
            // btnFinanzas
            // 
            this.btnFinanzas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnFinanzas.FlatAppearance.BorderSize = 0;
            this.btnFinanzas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinanzas.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinanzas.ForeColor = System.Drawing.Color.White;
            this.btnFinanzas.Location = new System.Drawing.Point(0, 475);
            this.btnFinanzas.Name = "btnFinanzas";
            this.btnFinanzas.Size = new System.Drawing.Size(253, 46);
            this.btnFinanzas.TabIndex = 3;
            this.btnFinanzas.Text = "Finanzas";
            this.btnFinanzas.UseVisualStyleBackColor = false;
            // 
            // btnControlGastos
            // 
            this.btnControlGastos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnControlGastos.FlatAppearance.BorderSize = 0;
            this.btnControlGastos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnControlGastos.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlGastos.ForeColor = System.Drawing.Color.White;
            this.btnControlGastos.Location = new System.Drawing.Point(1, 87);
            this.btnControlGastos.Name = "btnControlGastos";
            this.btnControlGastos.Size = new System.Drawing.Size(251, 46);
            this.btnControlGastos.TabIndex = 3;
            this.btnControlGastos.Text = "Control de Gastos";
            this.btnControlGastos.UseVisualStyleBackColor = false;
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btnProductos.FlatAppearance.BorderSize = 0;
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.Color.White;
            this.btnProductos.Location = new System.Drawing.Point(3, 243);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(250, 46);
            this.btnProductos.TabIndex = 3;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            // 
            // pContenidoo
            // 
            this.pContenidoo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.pContenidoo.Location = new System.Drawing.Point(264, 35);
            this.pContenidoo.Name = "pContenidoo";
            this.pContenidoo.Size = new System.Drawing.Size(1106, 663);
            this.pContenidoo.TabIndex = 2;
            this.pContenidoo.Paint += new System.Windows.Forms.PaintEventHandler(this.pContenidoo_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(9, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(1292, 697);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(63, 65);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(111)))), ((int)(((byte)(222)))));
            this.panel2.Location = new System.Drawing.Point(255, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 753);
            this.panel2.TabIndex = 0;
            // 
            // frmMenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.ClientSize = new System.Drawing.Size(1370, 788);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.BarraVertical);
            this.Controls.Add(this.BarraTitulo);
            this.Controls.Add(this.pContenidoo);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenuPrincipal";
            this.Text = "frmMenuPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenuPrincipal_Load);
            this.BarraTitulo.ResumeLayout(false);
            this.BarraTitulo.PerformLayout();
            this.BarraVertical.ResumeLayout(false);
            this.BarraVertical.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAgregarVenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Panel BarraVertical;
        private System.Windows.Forms.Button btnVentas;
        private System.Windows.Forms.Button btnControlUsuarios;
        private System.Windows.Forms.Button btnFinanzas;
        private System.Windows.Forms.Button btnControlGastos;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnRegistroVentas;
        private System.Windows.Forms.PictureBox btnAgregarVenta;
        private System.Windows.Forms.Panel pContenido;
        private System.Windows.Forms.Button btnRegistroProducto;
        private System.Windows.Forms.Button btnPrecioProductos;
        private System.Windows.Forms.Button btnCorteSemana;
        private System.Windows.Forms.Button btnCorteDia;
        private System.Windows.Forms.Button btnAdministrarUsuarios;
        private System.Windows.Forms.Button btnRegistroES;
        private System.Windows.Forms.Button btnLibroContable;
        private System.Windows.Forms.Panel pContenidoo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCerrarS;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnProveedores;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.Button btnCategoria;
        private System.Windows.Forms.Button btnInventario;
    }
}