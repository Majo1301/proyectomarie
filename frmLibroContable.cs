﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmLibroContable : Form
    {
        LibroContableManejador _libroContableManejador;
        public frmLibroContable()
        {
            InitializeComponent();
            _libroContableManejador = new LibroContableManejador();
        }

        public void seleccionarFecha()
        {
            if (dtpFechaI.Text.Contains("lunes"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(6).ToString();
            }
            else if (dtpFechaI.Text.Contains("martes"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(5).ToString();
            }
            else if (dtpFechaI.Text.Contains("miércoles"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(4).ToString();
            }
            else if (dtpFechaI.Text.Contains("jueves"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(3).ToString();
            }
            else if (dtpFechaI.Text.Contains("viernes"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(2).ToString();
            }
            else if (dtpFechaI.Text.Contains("sabado") || dtpFechaI.Text.Contains("sábado"))
            {
                dtpFechaI.Text = dtpFechaI.Value.AddDays(1).ToString();
            }
            else { }
        }
        private void frmLibroContable_Load(object sender, EventArgs e)
        {
            buscarLibroContable("");
            seleccionarFecha();

        }
        private void buscarLibroContable(string filtro)
        {
            dgvDatos.DataSource = _libroContableManejador.GetLibroContable(filtro);
        }
        private void buscarLibroContableFecha(string filtro)
        {
            dgvDatos.DataSource = _libroContableManejador.GetLibroContableFecha(filtro);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (cbBusqueda.Text == "Todas")
            {
                buscarLibroContable("");
            }
            else if (cbBusqueda.Text == "Fecha")
            {
                dtpFechaI.Visible = true;
                buscarLibroContableFecha(dtpFechaI.Text);
            }
            else { }
        }

        private void cbBusqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbBusqueda.Text == "Todas")
            {
                dtpFechaI.Visible = false;
                //Buscar
            }
            else if (cbBusqueda.Text == "Fecha")
            {
                dtpFechaI.Visible = true;
                //buscar por fecha;
            }
            else { }
        }

        private void dtpFechaI_ValueChanged(object sender, EventArgs e)
        {
            seleccionarFecha();
        }
    }
}
