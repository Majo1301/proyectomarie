﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmGastos : Form
    {
        GastosManejador _gastosManejador;

        /*void hover(bool a, bool b, bool c, bool d, bool e, bool f)
        {
            sVentas.Visible = a;
            sProductos.Visible = b;
            sControlGastos.Visible = c;
            sFinanzas.Visible = d;
            sControlUsuarios.Visible = e;
            sCortes.Visible = f;
        }*/

        /*TODOS LLEVAN ESTO*/
        private void limpiarcuadros()
        {
            txtId.Text = "0";
            txtMonto.Text = "";
            txtDescripcion.Text = "";
            cbTipo.Text = "";
            dtpFecha.Value = DateTime.Now;
            lblStatus.Text = "0";
        }
        private void controlarcuadros(bool activar)
        {
            //txtId.Enabled = activar;
            txtMonto.Enabled = activar;
            txtDescripcion.Enabled = activar;
            dtpFecha.Enabled = activar;
            cbTipo.Enabled = activar;
        }
        private void controlarbotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnAgregar.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            //btnModificar.Enabled = eliminar;
        }
        private void guardar()
        {
            _gastosManejador.Guardar(new Gastos
            {
                Id = int.Parse(txtId.Text),
                Monto = Convert.ToDouble(txtMonto.Text),
                Descripcion = txtDescripcion.Text,
                Tipo = cbTipo.Text,
                Fecha = dtpFecha.Text,
            }, int.Parse(lblStatus.Text)); ;
        }
        private void eliminar()
        {
            var id = dgvDatos.CurrentRow.Cells["id"].Value;
            _gastosManejador.Eliminar(Convert.ToInt32(id));
        }
        private void buscarGastos(string filtro)
        {
            dgvDatos.DataSource = _gastosManejador.GetGastos(filtro);
        }
        private void Modificar()
        {
            controlarcuadros(true);
            controlarbotones(false, true, true, false);
            txtId.Text = dgvDatos.CurrentRow.Cells["id"].Value.ToString();
            txtMonto.Text = dgvDatos.CurrentRow.Cells["monto"].Value.ToString();
            txtDescripcion.Text = dgvDatos.CurrentRow.Cells["descripcion"].Value.ToString();
            cbTipo.Text = dgvDatos.CurrentRow.Cells["tipo"].Value.ToString();
            dtpFecha.Text = dgvDatos.CurrentRow.Cells["fecha"].Value.ToString();
        }
        /*HASTA AQUI*/
        public frmGastos()
        {
            InitializeComponent();
            _gastosManejador = new GastosManejador();
            
        }
        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try
            {
                guardar();
                limpiarcuadros();
                buscarGastos("");
                gbtexto.Visible = false;
                dgvDatos.Visible = true;
                dgvDatos.Enabled = true;
                btnAgregar.Visible = true;
                btnEliminar.Visible = true;
                txtId.Enabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }

        private void frmGastos_Load(object sender, EventArgs e)
        {
            try
            {
                buscarGastos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show("no se pudo " + ex);
            }
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            cbTipo.Focus();
            gbtexto.Visible = true;
            dgvDatos.Enabled = false;
            btnAgregar.Visible = false;
            btnEliminar.Visible = false;
            txtId.Enabled = false;
            lblStatus.Text = "0";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el registro", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminar();
                    buscarGastos("");
                    txtId.Enabled = false;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
            gbtexto.Visible = false;
            dgvDatos.Visible = true;
            dgvDatos.Enabled = true;
            btnAgregar.Visible = true;
            btnEliminar.Visible = true;
            txtId.Enabled = false;
        }

        private void dgvDatos_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = "1";
                Modificar();
                buscarGastos("");
                dgvDatos.Visible = true;
                gbtexto.Visible = true;
                btnEliminar.Visible = false;
                btnAgregar.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cbTipo_Click(object sender, EventArgs e)
        {

        }

        private void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
           // buscarGastos("");
        }


    }
}
