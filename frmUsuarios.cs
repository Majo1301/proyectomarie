﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MarieCosmetics2._0
{
    public partial class frmUsuarios : Form
    {
        public frmUsuarios()
        {
            InitializeComponent();
            EstadoInicial();
            datosDTGusuarios();
        }
        //Esto llevan todos
        //SqlConnection conexion = new SqlConnection("data source=DESKTOPF2F; initial catalog=Marie_DB; user id=sa; password=123456789");
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        public void EstadoInicial()
        {
            lblStatus.Text = "0";
            Txt_nombreU.Text = "";
            Txt_domicilioU.Text = "";
            Txt_telU.Text = "";
            Txt_privilegioU.Text = "0";
            Txt_aliasU.Text = "";
            Txt_passwordU.Text = "";
        }
        public void datosDTGusuarios()
        {
            conexion.Open();
            var adaptador = new SqlDataAdapter("select * from usuarios", conexion);
            var command = new SqlCommandBuilder(adaptador);
            var ds = new DataSet();
            adaptador.Fill(ds);
            DtgListaUsuario.ReadOnly = true;
            DtgListaUsuario.DataSource = ds.Tables[0];
            conexion.Close();
        }
        public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }
        //Hasta aqui
        private void DtgListaUsuario_DoubleClick(object sender, EventArgs e)
        {
            lblStatus.Text = "1";
            try
            {
                Txt_idUsuario.Text = DtgListaUsuario.CurrentRow.Cells["id_usuario"].Value.ToString();
                Txt_nombreU.Text = DtgListaUsuario.CurrentRow.Cells["nombre_u"].Value.ToString();
                Txt_aliasU.Text = DtgListaUsuario.CurrentRow.Cells["alias_u"].Value.ToString();
                Txt_passwordU.Text = DtgListaUsuario.CurrentRow.Cells["password_u"].Value.ToString();
                Txt_domicilioU.Text = DtgListaUsuario.CurrentRow.Cells["domicilio_u"].Value.ToString();
                Txt_telU.Text = DtgListaUsuario.CurrentRow.Cells["telefono_u"].Value.ToString();
                Txt_privilegioU.Text = DtgListaUsuario.CurrentRow.Cells["tipo_u"].Value.ToString();
                controlbotones(false, false, true);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                controlbotones(true, true, false);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int idU = Convert.ToInt32(DtgListaUsuario.CurrentRow.Cells["id_usuario"].Value);
            try
            {
                string consulta = "delete from usuarios where id_usuario = '" + idU.ToString() + "'";
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                conexion.Open();
                cmd.ExecuteNonQuery();
                conexion.Close();
                MessageBox.Show("Usuario Eliminado Correctamente.");
                EstadoInicial();
                datosDTGusuarios();
                controlbotones(true,true,false);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            EstadoInicial();
            controlbotones(false, false, true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblStatus.Text == "0")
            {
                try
                {
                    string consulta = "INSERT INTO usuarios VALUES('" + Txt_nombreU.Text + "','" + Txt_aliasU.Text + "','" + Txt_passwordU.Text + "','" + Txt_domicilioU.Text + "','" + double.Parse(Txt_telU.Text) + "','" + Txt_privilegioU.Text + "')";
                    SqlCommand cmd = new SqlCommand(consulta, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Usuario Guardado Correctamente.");
                    EstadoInicial();
                    datosDTGusuarios();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                    controlbotones(true, true, false);
                }
            }
            else
            {
                try
                {
                    string update = "UPDATE usuarios set nombre_u = '" + Txt_nombreU.Text + "', alias_u = '" + Txt_aliasU.Text + "', password_u = '" + Txt_passwordU.Text + "', domicilio_u = '" + Txt_domicilioU.Text + "', telefono_u = '" + double.Parse(Txt_telU.Text) + "', tipo_u = '" + Txt_privilegioU.Text + "' where id_usuario = '" + double.Parse(Txt_idUsuario.Text) + "'";
                    SqlCommand cmd = new SqlCommand(update, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("usuario editado Correctamente.");
                    EstadoInicial();
                    datosDTGusuarios();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    controlbotones(true, true, false);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true,true, false);
        }
    }
}
