﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmRegistroVentas : Form
    {
        RegistroVentasManejador _registroVentasManejador;
        public frmRegistroVentas()
        {
            InitializeComponent();
            _registroVentasManejador = new RegistroVentasManejador();
        }
        public void limpiar(string texto,bool a, bool b, bool c, bool d)
        {
            cbBusqueda.Text = texto;
            dtpFechaI.Value = DateTime.Today.AddDays(-1);
            dtpFechaI.Visible = a;
            cbMovimiento.Visible = b;
            btnBuscar.Visible = c;
            cbMovimiento.Enabled = d;
            //limpiar("------- Selecciona forma de busqueda ------", false, false, false);
        }
        private void buscarVentas(string filtro)
        {
            dgvDatos.DataSource = _registroVentasManejador.GetVentas(filtro);
        }
        private void buscarFecha(string filtro)
        {
            dgvDatos.DataSource = _registroVentasManejador.GetFecha(filtro);
        }
        private void buscarFechas(string filtro, string filtro2)
        {
            dgvDatos.DataSource = _registroVentasManejador.GetFechas(filtro, filtro2);
        }
        private void cbBusqueda_SelectedValueChanged(object sender, EventArgs e)
        {
            dgvDatos.Columns.Clear();
            if (cbBusqueda.Text == "Todas")
            {
                limpiar("Todas", false, false, true, false);
            }
            else if (cbBusqueda.Text == "Fecha")
            {
                limpiar("Fecha", true, false, true,false);
            }
            else if (cbBusqueda.Text == "Fecha y movimiento")
            {
                limpiar("Fecha y movimiento", true,true,true,true);
            }
            else
            {
                limpiar("------- Selecciona forma de busqueda ------", false, false, true,false);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (cbBusqueda.Text == "Todas")
            {
                buscarVentas("");
            }
            else if (cbBusqueda.Text == "Fecha")
            {
                buscarFecha(dtpFechaI.Text);
            }
            else if (cbBusqueda.Text == "Fecha y movimiento")
            {
                buscarFechas(dtpFechaI.Text, cbMovimiento.Text);
            }
            else
            {}
        }
    }
}
