﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmPrecios : Form
    {
        ProductosManejador _productosManejador;
        PreciosManejador _preciosManejador;
        InventarioManejador _inventarioManejador;
        public frmPrecios()
        {
            InitializeComponent();
            _productosManejador = new ProductosManejador();
            _preciosManejador = new PreciosManejador();
            _inventarioManejador = new InventarioManejador();
        }
         public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }
        private void Modificar()
        {
            lblStatus.Text = "1";
            controlbotones(false, false, true);
            Txt_idprecio.Text = Dtg_precios.CurrentRow.Cells["id"].Value.ToString();
            Txt_nombre.Text = Dtg_precios.CurrentRow.Cells["producto"].Value.ToString();
            Txt_compra.Text = Dtg_precios.CurrentRow.Cells["precio_compra"].Value.ToString();
            Txt_venta.Text = Dtg_precios.CurrentRow.Cells["precio_venta"].Value.ToString();
            Txt_cantidad.Text = Dtg_precios.CurrentRow.Cells["cantidad"].Value.ToString();
            
        }
        private void Dtg_precios_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = "1";
                Modificar();
                buscarPrecio("");
                Dtg_precios.Visible = true;
                //gbtexto.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void guardar(int menu)
        {
            _preciosManejador.Guardar(new Precios
            {
                Id = int.Parse(Txt_idprecio.Text),
                Producto = Txt_nombre.Text,
                Precio_compra = Convert.ToDouble(Txt_compra.Text),
                Precio_venta = Convert.ToDouble(Txt_venta.Text),
                Cantidad = int.Parse(Txt_cantidad.Value.ToString()),
                Fecha = Fecha.Text,
            }, int.Parse(lblStatus.Text),menu); ;
        }
        private void guardarinventario()
        {
            _inventarioManejador.Guardar(new Inventario
            {
                Id = int.Parse(Txt_idprecio.Text),
                Nombre = Txt_nombre.Text,
                Cantidad = Convert.ToInt32(Txt_cantidad.Value),
            }, int.Parse(lblStatus.Text)); ;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlbotones(true, false, false);
            try
            {
                if (lblStatus.Text == "0")
                {
                    guardar(0);
                    guardarinventario();
                    buscarPrecio("");
                }
                else
                {
                    guardar(1);
                    guardarinventario();
                    buscarPrecio("");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            controlbotones(false, false,true);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            controlbotones(true, false, false);
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true, false, false);
        }

        private void buscarProducto(string filtro)
        {
            Dtg_precios.DataSource =_productosManejador.GetProductos(filtro);
        }
        private void obtenerProducto(string filtro)
        {
            Txt_nombre.DataSource = _productosManejador.GetProductos(filtro);
            Txt_nombre.DisplayMember = "nombre";
            Txt_nombre.ValueMember = "nombre";
        }
        private void buscarPrecio(string filtro)
        {
            Dtg_precios.DataSource = _preciosManejador.GetPrecios(filtro);
        }
        /*private void obtenerPrecio2()
        {
            Txt_nombre.DataSource = _preciosManejador.GetPrecios2();
            Txt_nombre.DisplayMember = "producto";
            Txt_nombre.ValueMember = "producto";
        }*/
        private void frmPrecios_Load(object sender, EventArgs e)
        {
            buscarPrecio("");
        }

        private void Txt_nombre_Click(object sender, EventArgs e)
        {
           /* if (lblStatus.Text == "0")
            {
                obtenerPrecio2();
            }
            else
            {*/
                obtenerProducto("");
            /*}*/
        }
    }
}
