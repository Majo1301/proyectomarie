﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmCorteDia : Form
    {
        CorteDiaManejador _corteDiaManejador;
        public frmCorteDia(string usuario) //agregar lo del tipo de usuario
        {
            InitializeComponent();
            _corteDiaManejador = new CorteDiaManejador();
            lblUsuarioo.Text = usuario;
        }
        private void limpiarcuadros()
        {
            txtId.Text = "0";
            txtMonto.Text = "0";
            dtpFecha.Value = DateTime.Now;
            lblStatus.Text = "0";
            if (lblUsuarioo.Text=="1")
            {
                dtpFecha.Enabled = true;
            }
        }
        private void controlarcuadros(bool activar)
        {
            //txtId.Enabled = activar;
            txtMonto.Enabled = activar;
            dtpFecha.Enabled = activar;
        }
        private void controlarbotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnAgregar.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            //btnModificar.Enabled = eliminar;
        }
        private void guardar()
        {
            try
            {
                _corteDiaManejador.Guardar(new CorteDia
                {
                    Id = int.Parse(txtId.Text),
                    Fecha = dtpFecha.Text,
                    Total = Convert.ToDouble(txtMonto.Text),
                }, int.Parse(lblStatus.Text)); ;
            }
            catch (Exception ex)
            {
            }
        }
        private void eliminar()
        {
            var id = dgvDatos.CurrentRow.Cells["id"].Value;
            _corteDiaManejador.Eliminar(Convert.ToInt32(id));
        }
        private void buscarCorteDia(string filtro)
        {
            dgvDatos.DataSource = _corteDiaManejador.GetCorteDia(filtro);
            //dgvDatos.Columns["gastos"].Visible = false;
            //dgvDatos.Columns["subtotal"].Visible = false;
        }
        private void Modificar()
        {
            controlarcuadros(true);
            controlarbotones(false, true, true, false);
            txtId.Text = dgvDatos.CurrentRow.Cells["id"].Value.ToString();
            dtpFecha.Text = dgvDatos.CurrentRow.Cells["fecha"].Value.ToString();
            txtMonto.Text = dgvDatos.CurrentRow.Cells["monto"].Value.ToString();

        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            //dtpFecha.Focus();
            gbtexto.Visible = true;
            dgvDatos.Enabled = false;
            btnAgregar.Visible = false;
            btnEliminar.Visible = false;
            txtId.Enabled = false;
            lblStatus.Text = "0";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el registro", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminar();
                    buscarCorteDia("");
                    txtId.Enabled = false;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
           // controlarbotones(false, false, false, true);
           // controlarcuadros(false);
            try
            {
                guardar();
                limpiarcuadros();
                buscarCorteDia("");
                gbtexto.Visible = false;
                dgvDatos.Visible = true;
                dgvDatos.Enabled = true;
               // btnAgregar.Visible = true;
              //  btnEliminar.Visible = true;
                txtId.Enabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
            gbtexto.Visible = false;
            dgvDatos.Visible = true;
            dgvDatos.Enabled = true;
            btnAgregar.Visible = true;
            btnEliminar.Visible = true;
            txtId.Enabled = false;
        }

        private void dgvDatos_DoubleClick(object sender, EventArgs e)
        {
           /* try
            {
                lblStatus.Text = "1";
                Modificar();
                buscarCorteDia("");
                dgvDatos.Visible = true;
                gbtexto.Visible = true;
                btnEliminar.Visible = false;
                btnAgregar.Visible = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }*/
        }

        private void frmCorteDia_Load(object sender, EventArgs e)
        {
           
            try
            {
                buscarCorteDia("");
            }
            catch (Exception ex)
            {
                MessageBox.Show("no se pudo " + ex);
            }
            //controlarbotones(false, false, false, false);
            //controlarcuadros(false);
            limpiarcuadros();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
