﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MarieCosmetics2._0
{
    public partial class frmProveedores : Form
    {
        public frmProveedores()
        {
            InitializeComponent();
            EstadoInicial();
            datosDTGproveedor();
        }
        //SqlConnection conexion = new SqlConnection("data source=DESKTOPF2F; initial catalog=Marie_DB; user id=sa; password=123456789");
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }
        public void EstadoInicial()
        {
            lblStatus.Text = "0";
            Txt_Id.Text = "";
            Txt_nomProvee.Text = "";
            Txt_direcionP.Text = "";
            Txt_telP.Text = "";
            Txt_contactoP.Text = "";
        }
        public void datosDTGproveedor()
        {
            conexion.Open();
            var adaptador = new SqlDataAdapter("select * from proveedor", conexion);
            var command = new SqlCommandBuilder(adaptador);
            var ds = new DataSet();
            adaptador.Fill(ds);
            DtgListaproveedor.ReadOnly = true;
            DtgListaproveedor.DataSource = ds.Tables[0];
            conexion.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(DtgListaproveedor.CurrentRow.Cells["id_proveedor"].Value);
            try
            {
                string consulta = "delete from proveedor where id_proveedor = '" + id.ToString() + "'";
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                conexion.Open();
                cmd.ExecuteNonQuery();
                conexion.Close();
                MessageBox.Show("Proveedor Eliminado Correctamente.");
                EstadoInicial();
                datosDTGproveedor();
                controlbotones(true, true, false);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
            controlbotones(true, true, false);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            EstadoInicial();
            controlbotones(false, false, true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblStatus.Text == "0")
            {
                try
                {
                    string consulta = "INSERT INTO proveedor VALUES('" + Txt_nomProvee.Text + "','" + Txt_direcionP.Text + "','" + double.Parse(Txt_telP.Text) + "','" + Txt_contactoP.Text + "')";
                    SqlCommand cmd = new SqlCommand(consulta, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Proveedor Guardado Correctamente.");
                    EstadoInicial();
                    datosDTGproveedor();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                    controlbotones(true, true, false);
                }
            }
            else 
            {
                try
                {
                    string update = "UPDATE proveedor set nombre_proveedor = '" + Txt_nomProvee.Text + "', direccion_proveedor = '" + Txt_direcionP.Text + "', telefono_proveedor = '" + double.Parse(Txt_telP.Text) + "', contacto_proveedor = '" + Txt_contactoP.Text + "' where id_proveedor = '" + double.Parse(Txt_Id.Text) + "'";
                    SqlCommand cmd = new SqlCommand(update, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Proveedor editado Correctamente.");
                    EstadoInicial();
                    datosDTGproveedor();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                    controlbotones(true, true, false);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true, true, false);
        }

        private void DtgListaproveedor_DoubleClick(object sender, EventArgs e)
        {
            lblStatus.Text = "1";
            try
            {
                Txt_Id.Text = DtgListaproveedor.CurrentRow.Cells["id_proveedor"].Value.ToString();
                Txt_nomProvee.Text = DtgListaproveedor.CurrentRow.Cells["nombre_proveedor"].Value.ToString();
                Txt_direcionP.Text = DtgListaproveedor.CurrentRow.Cells["direccion_proveedor"].Value.ToString();
                Txt_telP.Text = DtgListaproveedor.CurrentRow.Cells["telefono_proveedor"].Value.ToString();
                Txt_contactoP.Text = DtgListaproveedor.CurrentRow.Cells["contacto_proveedor"].Value.ToString();
                controlbotones(false, false, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                controlbotones(true, true, false);
            }
        }

        private void frmProveedores_Load(object sender, EventArgs e)
        {

        }

        private void DtgListaproveedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
