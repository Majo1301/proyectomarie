﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarieCosmetics2._0
{
    public partial class frmMenuPrincipal : Form
    {
        public frmMenuPrincipal(string tipo, string usuario)
        {
            InitializeComponent();
            lblTipo.Text = tipo;
            lblUsuario.Text = usuario;
        }
        //<a target="_blank" href="https://icons8.com/icon/4511GGVppfIx/ajustes">Ajustes</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
        private void panelContenido(object formhijo)
        {
            if (this.pContenidoo.Controls.Count > 0)
                this.pContenidoo.Controls.RemoveAt(0);
            Form fh = formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pContenidoo.Controls.Add(fh);
            this.pContenidoo.Tag = fh;
            fh.Show();
        }
        private void frmMenuPrincipal_Load(object sender, EventArgs e)
        {
            if (lblTipo.Text != "1")
            {
                btnProductos.Visible = false;
                btnFinanzas.Visible = false;
                btnRegistroES.Visible = false;
                btnRegistroProducto.Visible = false;
                btnPrecioProductos.Visible = false;
                btnProveedores.Visible = false;
                btnLibroContable.Visible = false;
                btnAdministrarUsuarios.Visible = false;
                btnControlUsuarios.Visible = false;
                btnCorteSemana.Visible = false;
                btnRegistroVentas.Enabled = false;
                btnCategoria.Visible = false;
                btnInventario.Visible = false;
            }
            else 
            {
                btnProductos.Visible = true;
                btnFinanzas.Visible = true;
                btnRegistroES.Visible = true;
                btnRegistroProducto.Visible = true;
                btnPrecioProductos.Visible = true;
                btnProveedores.Visible = true;
                btnLibroContable.Visible = true;
                btnAdministrarUsuarios.Visible = true;
                btnControlUsuarios.Visible = true;
                btnCorteSemana.Visible = true;
                btnRegistroVentas.Enabled = true;
                btnCategoria.Visible= true;
                btnInventario.Visible = true;
            }
        }
        private void btnRegistroVentas_MouseHover(object sender, EventArgs e)
        {
            btnRegistroVentas.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnRegistroVentas_MouseLeave(object sender, EventArgs e)
        {
            btnRegistroVentas.BackColor = Color.FromArgb(25, 25, 25);
        }



        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            btnAgregarVenta.BackColor = Color.FromArgb(64, 20, 107); 
        }

        private void btnAgregarVenta_MouseLeave(object sender, EventArgs e)
        { 
            btnAgregarVenta.BackColor = Color.FromArgb(48, 33, 63);
        }
        private void btnRegistroES_Click(object sender, EventArgs e)
        {
            panelContenido(new frmGastos());
        }

        private void btnAgregarVenta_MouseDown(object sender, MouseEventArgs e)
        {
            btnAgregarVenta.BackColor = Color.FromArgb(166, 111, 222);
        }

        private void btnAgregarVenta_MouseUp(object sender, MouseEventArgs e)
        {
            btnAgregarVenta.BackColor = Color.FromArgb(48, 33, 63);
        }
        private void lblCerrarS_MouseHover(object sender, EventArgs e)
        {
            lblCerrarS.ForeColor = Color.FromArgb(166, 111, 222);    
        }

        private void lblCerrarS_MouseLeave(object sender, EventArgs e)
        {
            lblCerrarS.ForeColor = Color.White;
        }

        private void lblCerrarS_Click(object sender, EventArgs e)
        {
            /*  DialogResult dialogResult = MessageBox.Show("Estas seguro de querer cerrar sesion?", "", MessageBoxButtons.YesNo);
              if (dialogResult == DialogResult.Yes)
              {
                  this.ShowInTaskbar = false;
                  this.Hide();
                  frmLogin m = new frmLogin();
                  m.ShowDialog();
                  m.ShowInTaskbar = true;
                  m.Show();
              }
              else if (dialogResult == DialogResult.No)
              {

              }*/
            this.ShowInTaskbar = false;
            this.Hide();
            frmLogin m = new frmLogin();
            m.ShowDialog();
            m.ShowInTaskbar = true;
            m.Show();
        }


        private void btnCorteDia_MouseHover(object sender, EventArgs e)
        {
            btnCorteDia.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnCorteSemana_MouseHover(object sender, EventArgs e)
        {
            btnCorteSemana.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnRegistroES_MouseHover(object sender, EventArgs e)
        {
            btnRegistroES.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnRegistroProducto_MouseHover(object sender, EventArgs e)
        {
            btnRegistroProducto.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnPrecioProductos_MouseHover(object sender, EventArgs e)
        {
            btnPrecioProductos.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnLibroContable_MouseHover(object sender, EventArgs e)
        {
            btnLibroContable.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnAdministrarUsuarios_MouseHover(object sender, EventArgs e)
        {
            btnAdministrarUsuarios.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnRegistroCorte_MouseLeave(object sender, EventArgs e)
        {
        }

        private void btnCorteDia_MouseLeave(object sender, EventArgs e)
        {
            btnCorteDia.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnCorteSemana_MouseLeave(object sender, EventArgs e)
        {
            btnCorteSemana.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnRegistroES_MouseLeave(object sender, EventArgs e)
        {
            btnRegistroES.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnRegistroProducto_MouseLeave(object sender, EventArgs e)
        {
            btnRegistroProducto.BackColor = Color.FromArgb(25, 25, 25);
        }


        private void btnPrecioProductos_MouseLeave(object sender, EventArgs e)
        {
            btnPrecioProductos.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnLibroContable_MouseLeave(object sender, EventArgs e)
        {
            btnLibroContable.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnAdministrarUsuarios_MouseLeave(object sender, EventArgs e)
        {
            btnAdministrarUsuarios.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnRegistroProducto_Click(object sender, EventArgs e)
        {
            panelContenido(new frmProductos());
        }

        private void btnRegistroVentas_Click(object sender, EventArgs e)
        {
            panelContenido(new frmRegistroVentas());
        }

        private void btnAdministrarUsuarios_Click(object sender, EventArgs e)
        {
            panelContenido(new frmUsuarios());
        }

        private void btnPrecioProductos_Click(object sender, EventArgs e)
        {
            panelContenido(new frmPrecios());
        }

        private void pContenidoo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnProveedores_MouseHover(object sender, EventArgs e)
        {
            btnProveedores.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnProveedores_MouseLeave(object sender, EventArgs e)
        {
            btnProveedores.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            panelContenido(new frmProveedores());
        }

        private void btnCorteSemana_Click(object sender, EventArgs e)
        {
            panelContenido(new frmCorteSemanal());
        }

        private void btnIncidente_Click(object sender, EventArgs e)
        {

        }

        private void btnCategoria_Click(object sender, EventArgs e)
        {
            panelContenido(new frmCategoria());
        }

        private void btnCorteDia_Click(object sender, EventArgs e)
        {
            panelContenido(new frmCorteDia(lblTipo.Text));
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            panelContenido(new frmInventario());
        }

        private void btnAgregarVenta_Click(object sender, EventArgs e)
        {
            panelContenido(new frmVentas());
        }

        private void btnLibroContable_Click(object sender, EventArgs e)
        {
            panelContenido(new frmLibroContable());
        }

        private void btnCategoria_MouseHover(object sender, EventArgs e)
        {
            btnCategoria.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnCategoria_MouseLeave(object sender, EventArgs e)
        {
            btnCategoria.BackColor = Color.FromArgb(25, 25, 25);
        }

        private void btnInventario_MouseHover(object sender, EventArgs e)
        {
            btnInventario.BackColor = Color.FromArgb(48, 33, 63);
        }

        private void btnInventario_MouseLeave(object sender, EventArgs e)
        {
            btnInventario.BackColor = Color.FromArgb(25, 25, 25);
        }
    }
}
