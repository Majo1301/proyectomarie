﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using AccesoDatos;

namespace MarieCosmetics2._0
{
    public partial class frmProductos : Form
    {
        public frmProductos()
        {
            InitializeComponent();
            EstadoIncial();
            datosDTGproductos();
        }
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        //SqlConnection conexion = new SqlConnection("data source=DESKTOPF2F; initial catalog=Marie_DB; user id=sa; password=123456789");
        SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        ComboBoxDatos Cbdatos = new ComboBoxDatos();
        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }
        public void datosDTGproductos()
        {
            conexion.Open();
            var adaptador = new SqlDataAdapter("select * from productos", conexion);
            var command = new SqlCommandBuilder(adaptador);
            var ds = new DataSet();
            adaptador.Fill(ds);
            dgvDatos.ReadOnly = true;
            dgvDatos.DataSource = ds.Tables[0];
            conexion.Close();
        }
        public void EstadoIncial()
        {
            Cbdatos.SeleccionarCategoria(Cmb_categoria);
            Lbl_k.Text = "";
            Cbdatos.Seleccionarproveedor(Cmb_proveedor);
            Lbl_proveedor.Text = "-";
            Txt_idproducto.Text = "";
            Txt_nombrep.Text = "";
            Txt_cantidad.Text = "0";
            lblStatus.Text = "0";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblStatus.Text == "1")//modificar
            {
                try
                {
                    string update = "UPDATE productos set nombre_producto = '" + Txt_nombrep.Text + "', proveedor_producto = '" + Cmb_proveedor.Text + "', categoria_producto = '" + Cmb_categoria.Text + "', cantidad_tope = '" + double.Parse(Txt_cantidad.Text) + "' where id_producto = '" + double.Parse(Txt_idproducto.Text) + "'";
                    SqlCommand cmd = new SqlCommand(update, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Producto editado Correctamente.");
                    EstadoIncial();
                    datosDTGproductos();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    controlbotones(true, true, false);
                }
            }
            else 
            {
                try
                {
                    string categoria = Cmb_categoria.Text;
                    string proveedor = Cmb_proveedor.Text;
                    string consulta = "INSERT INTO productos VALUES('" + Txt_nombrep.Text + "','" + proveedor + "','" + categoria + "','" + int.Parse(Txt_cantidad.Text) + "')";
                    SqlCommand cmd = new SqlCommand(consulta, conexion);
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    MessageBox.Show("Producto Guardado Correctamente.");
                    EstadoIncial();
                    datosDTGproductos();
                    controlbotones(true, true, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    controlbotones(true, true, false);
                }
            }
           
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgvDatos.CurrentRow.Cells["id_producto"].Value);
            try
            {
                string consulta = "delete from productos where id_producto = '" + id.ToString() + "'";
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                conexion.Open();
                cmd.ExecuteNonQuery();
                conexion.Close();
                MessageBox.Show("Producto Eliminado Correctamente.");
                EstadoIncial();
                datosDTGproductos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dgvDatos_DoubleClick(object sender, EventArgs e)
        {
          /*  lblStatus.Text = "1";
            try
            {
                Txt_idproducto.Text = dgvDatos.CurrentRow.Cells["id_producto"].Value.ToString();
                Txt_nombrep.Text = dgvDatos.CurrentRow.Cells["nombre_producto"].Value.ToString();
                Cmb_proveedor.Text = dgvDatos.CurrentRow.Cells["proveedor_producto"].Value.ToString();
                Cmb_categoria.Text = dgvDatos.CurrentRow.Cells["categoria_producto"].Value.ToString();
                Txt_cantidad.Text = dgvDatos.CurrentRow.Cells["cantidad_tope"].Value.ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }*/
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "0";
            EstadoIncial();
            controlbotones(false,false,true);
        }

        private void Cmb_proveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_proveedor.SelectedIndex > 0)
            {
                string[] valores = Cbdatos.captar_infoproveedor(Cmb_proveedor.Text);
                Lbl_proveedor.Text = valores[0];
            }
        }

        private void Cmb_categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_categoria.SelectedIndex > 0)
            {
                string[] valores = Cbdatos.captar_infocategoria(Cmb_categoria.Text);
                Lbl_k.Text = valores[0];
            }
        }

        public void controlbotones(bool a, bool b, bool c) 
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlbotones(true, true, false);
        }

        private void frmProductos_Load(object sender, EventArgs e)
        {

        }
    }
}
