﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class LibroContable
    {
        private int id;
        private string fecha;
        private double total_ventas;
        private double total_menos_gastos;
        private double total_inventario;
        private double ganancia;

        public int Id { get => id; set => id = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public double Total_ventas { get => total_ventas; set => total_ventas = value; }
        public double Total_menos_gastos { get => total_menos_gastos; set => total_menos_gastos = value; }
        public double Total_inventario { get => total_inventario; set => total_inventario = value; }
        public double Ganancia { get => ganancia; set => ganancia = value; }
    }
}
