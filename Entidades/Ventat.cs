﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Ventat
    {
        private int id;
        private int idp;
        private double subtotal;
        private double total;

        public int Id { get => id; set => id = value; }
        public int Idp { get => idp; set => idp = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Total { get => total; set => total = value; }
    }
}
