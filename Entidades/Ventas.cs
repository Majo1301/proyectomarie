﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Ventas
    {
        private int id;
        private int idp;
        private string producto;
        private string fecha;
        private int cantidad;
        private double precio;
        private double total;
        private string movimiento;

        public int Id { get => id; set => id = value; }
        public int Idp { get => idp; set => idp = value; }
        public string Producto { get => producto; set => producto = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }
        public double Total { get => total; set => total = value; }
        public string Movimiento { get => movimiento; set => movimiento = value; }
    }
}
