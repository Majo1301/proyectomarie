﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Gastos
    {
        private int id;
        private string tipo;
        private string descripcion;
        private double monto;
        private string fecha;

        public int Id { get => id; set => id = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double Monto { get => monto; set => monto = value; }
        public string Fecha { get => fecha; set => fecha = value; }
    }
}
