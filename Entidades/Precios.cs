﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Precios
    {
        private int id;
        private string producto;
        private double precio_compra;
        private double precio_venta;
        private int cantidad;
        private string fecha;

        public int Id { get => id; set => id = value; }
        public string Producto { get => producto; set => producto = value; }
        public double Precio_compra { get => precio_compra; set => precio_compra = value; }
        public double Precio_venta { get => precio_venta; set => precio_venta = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public string Fecha { get => fecha; set => fecha = value; }
    }
}
