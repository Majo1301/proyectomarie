﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class CorteDia
    {
        private int id;
        private string fecha;
        private double total;
        private double subtotal;
        private double gastos;


        public int Id { get => id; set => id = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public double Total { get => total; set => total = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Gastos { get => gastos; set => gastos = value; }
    }
}
