﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace AccesoDatos
{
    public class ComboBoxDatos
    {

        public ComboBoxDatos(/*ComboBox categoria, string categorias, ComboBox proveedor, string provedors*/)
        {
            //conexion = new ConexionAccesoDatos("localhost", "root", "", "mariecosmetics", true/*3306*/);
        }
        //SqlConnection conexion = new SqlConnection("data source=DESKTOPF2F; initial catalog=Marie_DB; user id=sa; password=123456789");
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        public void SeleccionarCategoria(ComboBox cb)
        {
            cb.Items.Clear();
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM categoria", conexion);
            SqlDataReader dr = query.ExecuteReader();

            while (dr.Read())
            {
                cb.Items.Add(dr[1].ToString());
            }

            conexion.Close();
            cb.Items.Insert(0, "Selecciona categoría");
            cb.SelectedIndex = 0;

        }

        public string[] captar_infocategoria(string categoria)
        {
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM categoria WHERE tipo_categoria='" + categoria + "'", conexion);
            SqlDataReader dr = query.ExecuteReader();
            string[] resultado = null;

            while (dr.Read())
            {
                string[] valores = { dr[2].ToString() };
                resultado = valores;
            }
            conexion.Close();
            return resultado;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Seleccionarproveedor(ComboBox cb)
        {
            cb.Items.Clear();
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM proveedor", conexion);
            SqlDataReader dr = query.ExecuteReader();

            while (dr.Read())
            {
                cb.Items.Add(dr[1].ToString());
            }

            conexion.Close();
            cb.Items.Insert(0, "Selecciona Proveedor");
            cb.SelectedIndex = 0;

        }

        public string[] captar_infoproveedor(string proveedor)
        {
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM proveedor WHERE nombre_proveedor='" + proveedor + "'", conexion);
            SqlDataReader dr = query.ExecuteReader();
            string[] resultado = null;

            while (dr.Read())
            {
                string[] valores = { dr[4].ToString() };
                resultado = valores;
            }
            conexion.Close();
            return resultado;
        }
        //----------------------------------------------------
        public ComboBox SeleccionarProductoModificar(ComboBox cb)
        {
            cb.Items.Clear();
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT DISTINCT nombre_producto from precios", conexion);
            SqlDataReader dr = query.ExecuteReader();

            while (dr.Read())
            {
                cb.Items.Add(dr[1].ToString());
            }

            conexion.Close();
            cb.Items.Insert(0, "Selecciona produducto");
            cb.SelectedIndex = 0;
            return cb;
        }
        public string[] captar_infoproductoModificar(string categoria)
        {
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT DISTINCT nombre_producto from precios WHERE nombre_producto='" + categoria + "'", conexion);
            //SqlCommand query = new SqlCommand("SELECT nombre_producto FROM productos where nombre_producto like ('%" + categoria + "% ');", conexion);
            SqlDataReader dr = query.ExecuteReader();
            string[] resultado = null;

            while (dr.Read())
            {
                string[] valores = { dr[2].ToString() };
                resultado = valores;
            }
            conexion.Close();
            return resultado;
        }

        public ComboBox SeleccionarProducto(ComboBox cb)
        {
            cb.Items.Clear();
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM productos", conexion);
            SqlDataReader dr = query.ExecuteReader();

            while (dr.Read())
            {
                cb.Items.Add(dr[1].ToString());
            }

            conexion.Close();
            cb.Items.Insert(0, "Selecciona producto");
            cb.SelectedIndex = 0;
            return cb;
        }
        public string[] captar_infoproducto(string categoria)
        {
            conexion.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM productos WHERE nombre_producto='" + categoria + "'", conexion);
            //SqlCommand query = new SqlCommand("SELECT nombre_producto FROM productos where nombre_producto like ('%" + categoria + "% ');", conexion);
            SqlDataReader dr = query.ExecuteReader();
            string[] resultado = null;

            while (dr.Read())
            {
                string[] valores = { dr[2].ToString() };
                resultado = valores;
            }
            conexion.Close();
            return resultado;
        }

    }
}
