﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatos;
using System.Data;

namespace AccesoDatos
{
    public class InventarioAD
    {
        ConexionAccesoDatos conexion;
        public InventarioAD()
        {
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(Inventario inventario, int lbl) /*agregar subtotal*/
        {
            if (lbl != 1)
            {
                string consulta = string.Format("EXEC inventario_p '{0}','{1}','{2}';", 0, inventario.Nombre, inventario.Cantidad);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //Insertar
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("EXEC inventario_p '{0}','{1}','{2}';", 0, inventario.Nombre, inventario.Cantidad);
                conexion.EjecutarConsulta(consulta);

            }
        }
        public List<Inventario> GetInventario(string filtro)
        {
            var ListaInventario = new List<Inventario>();
            var ds = new DataSet();
            string consulta = "select * from stock where id_stock like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "stock");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var inventario = new Inventario
                {
                    Id = Convert.ToInt32(row["id_stock"]),
                    Nombre = row["nombre_producto"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                };
                ListaInventario.Add(inventario);
            }
            return ListaInventario;
        }
        public void Compra(string nombre, int cantidad)
        {
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("update stock set cantidad = cantidad-'{1}' where nombre_producto='{0}'", nombre, cantidad);
                conexion.EjecutarConsulta(consulta);
        }
        public void CompraEliminar(string nombre, int cantidad)
        {
            //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
            string consulta = string.Format("update stock set cantidad = cantidad+'{1}' where nombre_producto='{0}'", nombre, cantidad);
            conexion.EjecutarConsulta(consulta);
        }
    }
}
