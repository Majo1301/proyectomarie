﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class CorteSemanalAD
    {
        ConexionAccesoDatos conexion;
        public CorteSemanalAD()
        {
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(CorteSemanal corteSemanal, int lbl)
        {
            try
            {
                if (lbl != 1)
                {
                    string consulta = string.Format("EXEC corte_semanal_p '{0}';", corteSemanal.Fecha);
                    conexion.EjecutarConsulta(consulta);
                }
            }
            catch (Exception ex)
            {
            }
        }
        public List<CorteSemanal> GetCortSemanal(string filtro)
        {
            var ListaCorteSemanal = new List<CorteSemanal>();
            var ds = new DataSet();
            string consulta = "select * from corte_semanal where id_corte_semanal like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "corte_semanal");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var corteSemanal = new CorteSemanal
                {
                    Id = Convert.ToInt32(row["id_corte_semanal"]),
                    Fecha = row["fecha"].ToString(),
                    Fechas = row["fechas"].ToString(),
                    Total = Convert.ToDouble(row["total"]),
                    Subtotal = Convert.ToDouble(row["subtotal"]),
                    Gastos = Convert.ToDouble(row["total_gastos"]),
                };
                ListaCorteSemanal.Add(corteSemanal);
            }
            return ListaCorteSemanal;
        }
    } 
}
