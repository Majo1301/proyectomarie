﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class GastosAD
    {
        ConexionAccesoDatos conexion;
        public GastosAD()
        {
            //conexion = new ConexionAccesoDatos("localhost", "root", "", "mariecosmetics", true/*3306*/);
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        public void Guardar(Gastos gastos, int lbl)
        {
            if (lbl != 1)
            {
                string consulta = string.Format("insert into gastos values('{0}','{1}','{2}','{3}')", gastos.Tipo, gastos.Descripcion, gastos.Monto, gastos.Fecha);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //Insertar
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("update gastos set tipo_gasto='{1}',descripcion='{2}',monto='{3}',fecha='{4}' where id_gastos='{0}'", gastos.Id, gastos.Tipo, gastos.Descripcion, gastos.Monto, gastos.Fecha);
                conexion.EjecutarConsulta(consulta);

            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from gastos where id_gastos={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Gastos> GetGastos(string filtro)
        {
            var ListaGastos = new List<Gastos>();
            var ds = new DataSet();
            string consulta = "select * from gastos where id_gastos like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "gastos");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var gastos = new Gastos
                {
                    Id = Convert.ToInt32(row["id_gastos"]),
                    Tipo = row["tipo_gasto"].ToString(),
                    Descripcion = row["descripcion"].ToString(),
                    Monto= Convert.ToDouble(row["monto"]),
                    Fecha = row["fecha"].ToString(),
                };
                ListaGastos.Add(gastos);
            }
            return ListaGastos;
        }
    }
}
