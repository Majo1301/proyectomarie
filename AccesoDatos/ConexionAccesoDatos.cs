﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
//sing MySql.Data.MySqlClient;
using System.Data;

namespace AccesoDatos
{
    public class ConexionAccesoDatos
    {
        private SqlConnection conn;
        //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        /* public ConexionAccesoDatos(string servidor, string usuario, string password, string database, bool seguridad)//cotr y doble tabulador
         {
             //instancia de msql
             SqlConnectionStringBuilder CadenaConexion = new SqlConnectionStringBuilder();//OBJETO DE TIPO STRING BUILDER
             CadenaConexion.DataSource = servidor;
             CadenaConexion.UserID = usuario;
             CadenaConexion.Password = password;
             CadenaConexion.InitialCatalog = database;
             CadenaConexion.IntegratedSecurity = seguridad;

             conn = new SqlConnection(CadenaConexion.ToString());
         }*/
        
         public ConexionAccesoDatos(string cadenada)//cotr y doble tabulador
         {
             //instancia de msql
             SqlConnectionStringBuilder CadenaConexion = new SqlConnectionStringBuilder();//OBJETO DE TIPO STRING BUILDER

             conn = new SqlConnection(cadenada);
         }
        public void EjecutarConsulta(string consulta)
        {
            conn.Open();
            var command = new SqlCommand(consulta, conn);
            command.ExecuteNonQuery();
            conn.Close();

        }
        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(consulta, conn);//llenar el dataset con el adaptador
            da.Fill(ds, tabla);
            return ds;
        }
    }
}
