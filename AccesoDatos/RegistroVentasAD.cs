﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class RegistroVentasAD
    {
        ConexionAccesoDatos conexion;
        public RegistroVentasAD()
        {
            // conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(RegistroVentas registroVentas, int lbl)
        {
            if (lbl != 1)
            {
                string consulta = string.Format("insert into ventas values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", registroVentas.Idp,registroVentas.Producto,registroVentas.Fecha,registroVentas.Cantidad,registroVentas.Producto,registroVentas.Total,registroVentas.Movimiento);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //Insertar
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("update gastos set id_venta='{1}',nombre_producto='{2}',fecha='{3}',cantidad='{4}',precio='{5}',total='{6}',movimiento='{7}' where id_venta_p1='{0}'", registroVentas.Id, registroVentas.Idp, registroVentas.Producto, registroVentas.Fecha, registroVentas.Cantidad, registroVentas.Producto, registroVentas.Total, registroVentas.Movimiento);
                conexion.EjecutarConsulta(consulta);

            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from ventas where id_venta_p1={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<RegistroVentas> GetVentas(string filtro)
        {
            var ListaRegistroVentas = new List<RegistroVentas>();
            var ds = new DataSet();
            string consulta = "select * from ventas where id_venta_p1 like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "ventas");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var registroVentas = new RegistroVentas
                {
                    Id = Convert.ToInt32(row["id_venta_p1"].ToString()),
                    Idp = Convert.ToInt32(row["id_venta"].ToString()),
                    Producto = row["nombre_producto"].ToString(),
                    Fecha = row["fecha"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Precio= Convert.ToDouble(row["precio"].ToString()),
                    Total=Convert.ToDouble(row["total"].ToString()),
                    Movimiento=row["movimiento"].ToString(),
                };
                ListaRegistroVentas.Add(registroVentas);
            }
            return ListaRegistroVentas;
        }
        public List<RegistroVentas> GetFechas(string filtro, string filtro2)
        {
            var ListaRegistroVentas = new List<RegistroVentas>();
            var ds = new DataSet();
            string consulta = "select * from ventas where fecha like ('%" + filtro + "%') and movimiento like ('%" + filtro2 + "%') ";
            ds = conexion.ObtenerDatos(consulta, "ventas");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var registroVentas = new RegistroVentas
                {
                    Id = Convert.ToInt32(row["id_venta_p1"].ToString()),
                    Idp = Convert.ToInt32(row["id_venta"].ToString()),
                    Producto = row["nombre_producto"].ToString(),
                    Fecha = row["fecha"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Precio = Convert.ToDouble(row["precio"].ToString()),
                    Total = Convert.ToDouble(row["total"].ToString()),
                    Movimiento = row["movimiento"].ToString(),
                };
                ListaRegistroVentas.Add(registroVentas);
            }
            return ListaRegistroVentas;
        }
        public List<RegistroVentas> GetFecha(string filtro)
        {
            var ListaRegistroVentas = new List<RegistroVentas>();
            var ds = new DataSet();
            string consulta = "select * from ventas where fecha = '" + filtro + "'";
            ds = conexion.ObtenerDatos(consulta, "ventas");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var registroVentas = new RegistroVentas
                {
                    Id = Convert.ToInt32(row["id_venta_p1"].ToString()),
                    Idp = Convert.ToInt32(row["id_venta"].ToString()),
                    Producto = row["nombre_producto"].ToString(),
                    Fecha = row["fecha"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Precio = Convert.ToDouble(row["precio"].ToString()),
                    Total = Convert.ToDouble(row["total"].ToString()),
                    Movimiento = row["movimiento"].ToString(),
                };
                ListaRegistroVentas.Add(registroVentas);
            }
            return ListaRegistroVentas;
        }
    }
}
