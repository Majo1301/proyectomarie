﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class CorteDiaAD
    {
        ConexionAccesoDatos conexion;
        public CorteDiaAD()
        {
            //SqlConnection conexion = new SqlConnection("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            //SqlConnection conexion = new SqlConnection("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(CorteDia corteDia, int lbl)
        {
            try
            {
                if (lbl != 1)
                {
                    //string consulta = string.Format("insert into corte_dia values('{0}','{1}')", corteDia.Fecha, corteDia.Total);
                    //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                    string consulta = string.Format("EXEC corte_dia_p '{0}';", corteDia.Fecha);

                    //string consulta = string.Format("Exce todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                    conexion.EjecutarConsulta(consulta);
                }
                else
                {
                    //Insertar
                    //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                    string consulta = string.Format("update corte_dia set fecha='{1}',total='{2}',subtotal='{3}',total_gastos='{4}', where id_corte_dia='{0}'", corteDia.Id, corteDia.Fecha, corteDia.Total,corteDia.Subtotal,corteDia.Gastos);
                    conexion.EjecutarConsulta(consulta);

                }
            }
            catch (Exception ex)
            {
            }
        }

        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from corte_dia where id_corte_dia={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<CorteDia> GetCorteDia(string filtro)
        {
            var ListaCorteDia = new List<CorteDia>();
            var ds = new DataSet();
            string consulta = "select * from corte_dia where id_corte_dia like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "corte_dia");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var corteDia = new CorteDia
                {
                    Id = Convert.ToInt32(row["id_corte_dia"]),
                    Fecha = row["fecha"].ToString(),
                    Total = Convert.ToDouble(row["total"]),
                    Subtotal = Convert.ToDouble(row["subtotal"]),
                    Gastos = Convert.ToDouble(row["total_gastos"]),
                };
                ListaCorteDia.Add(corteDia);
            }
            return ListaCorteDia;
        }
    }
}
