﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class ProductosSD
    {
        ConexionAccesoDatos conexion;
        public ProductosSD()
        {
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            // conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(Productos productos, int lbl)
        {
            try
            {
                    string consulta = string.Format("EXEC corte_dia_p '{0}';", productos.Nombre);
                    conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {}
        }
        public List<Productos> GetProducto(string filtro)
        {
            var ListaProductos = new List<Productos>();
            var ds = new DataSet();
            string consulta = "select * from productos where id_producto like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "productos");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var productos = new Productos
                {
                    Id = Convert.ToInt32(row["id_producto"]),
                    Nombre = row["nombre_producto"].ToString(),
                    Proveedor = row["proveedor_producto"].ToString(),
                    Categoria = row["categoria_producto"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad_tope"].ToString()),
                };
                ListaProductos.Add(productos);
            }
            return ListaProductos;
        }
        public List<Productos> getP(string filtro)
        {
            var ListProductos = new List<Productos>();
            var ds = new DataSet();
            string consulta = "select * from productos where id_producto like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "productos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var productos = new Productos
                {
                    Id = Convert.ToInt32(row["id_producto"]),
                    Nombre = row["nombre_producto"].ToString(),
                    Proveedor = row["proveedor_producto"].ToString(),
                    Categoria = row["categoria_producto"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad_tope"].ToString()),
                };
                ListProductos.Add(productos);
            }
            return ListProductos;
        }
    }
}
