﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class LibroContableAD
    {
        ConexionAccesoDatos conexion;
        public LibroContableAD()
        {
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        public void Guardar( int lbl, string fecha)
        {
                string consulta = string.Format("EXEC libro_contable_p '{0}'", fecha);
                conexion.EjecutarConsulta(consulta);
        }
        public List<LibroContable> GetLibroContable(string filtro)
        {
            var ListaLibroContable= new List<LibroContable>();
            var ds = new DataSet();
            string consulta = "select * from libro_contable where id_libro like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "libro_contable");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var libroContable = new LibroContable
                {
                    Id = Convert.ToInt32(row["id_libro"]),
                    Fecha = row["fecha"].ToString(),
                    Total_ventas = Convert.ToDouble(row["total_ventas"]),
                    Total_menos_gastos = Convert.ToDouble(row["total_menos_gastos"]),
                    Total_inventario = Convert.ToDouble(row["total_inventario"]),
                    Ganancia= Convert.ToDouble(row["ganancia"]),
                };
                ListaLibroContable.Add(libroContable);
            }
            return ListaLibroContable;
        }
        public List<LibroContable> GetLibroContableFecha(string filtro)
        {
            var ListaLibroContable = new List<LibroContable>();
            var ds = new DataSet();
            string consulta = "select * from libro_contable where fecha like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "libro_contable");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var libroContable = new LibroContable
                {
                    Id = Convert.ToInt32(row["id_libro"]),
                    Fecha = row["fecha"].ToString(),
                    Total_ventas = Convert.ToDouble(row["total_ventas"]),
                    Total_menos_gastos = Convert.ToDouble(row["total_menos_gastos"]),
                    Total_inventario = Convert.ToDouble(row["total_inventario"]),
                    Ganancia = Convert.ToDouble(row["ganancia"]),
                };
                ListaLibroContable.Add(libroContable);
            }
            return ListaLibroContable;
        }
    }
}
