﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;


namespace AccesoDatos
{
    public class VentasAD
    {
        ConexionAccesoDatos conexion;
        public VentasAD()
        {
            //conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(Ventas ventas, int lbl, string movimiento)
        {
           /* if (lbl == 0) //trucha para agregar y demas
            {*/
                string consulta = string.Format("insert into ventas values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", ventas.Idp,ventas.Producto,ventas.Fecha,ventas.Cantidad,ventas.Precio,ventas.Total,movimiento);
                conexion.EjecutarConsulta(consulta);
           /* }
            else
            {
                //Insertar
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("update venta_total set id_ventap='{1}',total='{2}' where id_venta='{0}'");
                conexion.EjecutarConsulta(consulta);

            }*/
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("delete from ventas where id_venta_p1={0}", id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Ventas> GetVentas(string filtro)
        {
            var ListaVentas = new List<Ventas>();
            var ds = new DataSet();
            string consulta = "select * from ventas where id_venta like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "ventas");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var ventas= new Ventas
                {
                    Id = Convert.ToInt32(row["id_venta_p1"].ToString()),
                    Idp = Convert.ToInt32(row["id_venta"].ToString()),
                    Producto = row["nombre_producto"].ToString(),
                    Fecha = row["fecha"].ToString(),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Precio = Convert.ToDouble(row["precio"].ToString()),
                    Total = Convert.ToDouble(row["total"].ToString()),
                    Movimiento = row["movimiento"].ToString(),
                };
                ListaVentas.Add(ventas);
            }
            return ListaVentas;
        }

        /*  public string GetPrecioTotalVenta(string filtro)
          {
              var ListaPrecios = new List<Precios>();
              var ds = new DataSet();
              string consulta = "SELECT TOP 1 * FROM venta_total where id_ventap='" + filtro + "' ORDER BY id_venta DESC";
              ds = conexion.ObtenerDatos(consulta, "venta_total");// el data set se recoorre para la lista
              var dt = new DataTable();
              dt = ds.Tables[0];//queremos la tabla 0
              string variable = "";
              foreach (DataRow row in dt.Rows)
              {
                  variable = row["total"].ToString();
              };
              return variable;
          }*/
    }
}
