﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class PrecioAD
    {
        ConexionAccesoDatos conexion;
        public PrecioAD()
        {
            // conexion = new ConexionAccesoDatos("DESKTOPF2F", "sa", "123456789", "Marie_DB", true/*3306*/);
            //conexion = new ConexionAccesoDatos("Server=tcp:marieserver.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=admin_sa;Password=Usuariom1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            conexion = new ConexionAccesoDatos("Server=tcp:perla2021.database.windows.net,1433;Initial Catalog=Marie_DB_2.0;Persist Security Info=False;User ID=perla;Password=P123456789p;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        public void Guardar(Precios precios, int lbl, int menu)
        {
            if (lbl != 1)
            {
                string consulta = string.Format("EXEC precio_producto_stock_p '"+ menu + "','" + precios.Id + "','" + precios.Producto + "','" + precios.Precio_compra + "','" + precios.Precio_venta + "','" + precios.Cantidad + "','" + precios.Fecha + "'");
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //Insertar
                //string consulta = string.Format("call todohotel(null,'{1}','{2}','{3}')", hotel.Folio, hotel.Nombre, hotel.Direccion, hotel.Telefono);
                string consulta = string.Format("EXEC precio_producto_stock_modificar_p '" + menu + "','" + precios.Id + "','" + precios.Producto + "','" + precios.Precio_compra + "','" + precios.Precio_venta + "','" + precios.Cantidad + "','" + precios.Fecha + "'");
                conexion.EjecutarConsulta(consulta);

            }
        }
        public List<Precios> GetPrecio(string filtro)
        {
            var ListaPrecios = new List<Precios>();
            var ds = new DataSet();
            string consulta = "select * from precios where id_precios like ('%" + filtro + "%')";
            ds = conexion.ObtenerDatos(consulta, "precios");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var precio = new Precios
                {
                    Id = Convert.ToInt32(row["id_precios"]),
                    Producto = row["nombre_producto"].ToString(),
                    Precio_compra = Convert.ToDouble(row["precio_compra"].ToString()),
                    Precio_venta = Convert.ToDouble(row["precio_venta"].ToString()),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Fecha = row["fecha"].ToString(),
                };
                ListaPrecios.Add(precio);
            }
            return ListaPrecios;
        }
        public List<Precios> GetPrecioVenta(string filtro)
        {
            var ListaPrecios = new List<Precios>();
            var ds = new DataSet();
            string consulta = "SELECT TOP 1 * FROM precios where nombre_producto='"+filtro+"' ORDER BY id_precios DESC";
            ds = conexion.ObtenerDatos(consulta, "precios");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var precio = new Precios
                {
                    Id = Convert.ToInt32(row["id_precios"]),
                    Producto = row["nombre_producto"].ToString(),
                    Precio_compra = Convert.ToDouble(row["precio_compra"].ToString()),
                    Precio_venta = Convert.ToDouble(row["precio_venta"].ToString()),
                    Cantidad = Convert.ToInt32(row["cantidad"].ToString()),
                    Fecha = row["fecha"].ToString(),
                };
                ListaPrecios.Add(precio);
            }
            return ListaPrecios;
        }

        public string GetPrecioVentaA(string filtro)
        {
            var ListaPrecios = new List<Precios>();
            var ds = new DataSet();
            string consulta = "SELECT TOP 1 * FROM precios where nombre_producto='" + filtro + "' ORDER BY id_precios DESC";
            ds = conexion.ObtenerDatos(consulta, "precios");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0
            string variable = "";
                foreach (DataRow row in dt.Rows)
                {
                variable = row["precio_venta"].ToString();
                };
            return variable;
        }
        public string GetPrecioVentaB(string filtro)
        {
            var ListaPrecios = new List<Precios>();
            var ds = new DataSet();
            string consulta = "SELECT TOP 1 * FROM precios where nombre_producto='" + filtro + "' ORDER BY id_precios DESC";
            ds = conexion.ObtenerDatos(consulta, "precios");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0
            string variable = "";
            foreach (DataRow row in dt.Rows)
            {
                variable = row["precio_compra"].ToString();
            };
            return variable;
        }
    }
}
