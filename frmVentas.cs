﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios;
using Entidades;

namespace MarieCosmetics2._0
{
    public partial class frmVentas : Form
    {
        ProductosManejador _productosManejador;
        VentatManejador _ventatManejador;
        VentasManejador _ventasManejador;
        PreciosManejador _preciosManejador;
        InventarioManejador _inventarioManejador;
        public frmVentas()
        {
            InitializeComponent();
            _productosManejador = new ProductosManejador();
            _preciosManejador = new PreciosManejador();
            _ventatManejador = new VentatManejador();
            _ventasManejador = new VentasManejador();
            _inventarioManejador = new InventarioManejador();
        }
        public void controlbotones(bool a, bool b, bool c)
        {
            btnAgregar.Visible = a;
            btnEliminar.Visible = b;
            gbtexto.Visible = c;
        }
        /*private void Modificar()
         {
             lblStatus.Text = "1";
             controlbotones(false, false, true);
             Txt_idprecio.Text = Dtg_precios.CurrentRow.Cells["id"].Value.ToString();
             Txt_nombre.Text = Dtg_precios.CurrentRow.Cells["producto"].Value.ToString();
             Txt_compra.Text = Dtg_precios.CurrentRow.Cells["precio_compra"].Value.ToString();
             Txt_venta.Text = Dtg_precios.CurrentRow.Cells["precio_venta"].Value.ToString();
             Txt_cantidad.Text = Dtg_precios.CurrentRow.Cells["cantidad"].Value.ToString();

         }*/
        
        private void obtenerPrecioVenta(string filtro) //-------------------------------
        {
            lblCosto.Text= _preciosManejador.GetPrecioVentaA(filtro);
        }
        private void obtenerPrecioVentaB(string filtro) //-------------------------------
        {
            lblCostoCompra.Text = _preciosManejador.GetPrecioVentaB(filtro);
        }
        private string obtenerPrecioVentaC(string filtro) //-------------------------------
        {
            string x = _preciosManejador.GetPrecioVentaB(filtro);
            return x;
        }

        private void obtenerUltimaVenta() //saber cual es el id de la ultima venta
        {

            try
            {
                txtId.Text = (Convert.ToInt32(_ventatManejador.GetUltimaVenta()) + 1).ToString();
            }
            catch (Exception)
            {
                txtId.Text = "1";
            }
            
        }
        private void obtenerProducto(string filtro)
        {
            txtProducto.DataSource = _productosManejador.GetProductos(filtro);
            txtProducto.DisplayMember = "nombre";
            txtProducto.ValueMember = "nombre";
        }
        private void obtenerPrecioVentaCosto(string filtro)
        {
            if (txtProducto.SelectedValue.ToString() != "Entidades.Productos")
            {
                obtenerPrecioVenta(txtProducto.SelectedValue.ToString());
            }
        }
        private void label10_Click(object sender, EventArgs e)
        {

        }
        private void buscarUltimaVenta(string filtro)
        {
            try
            {
                dgvDatos.DataSource = _ventasManejador.GetVentas(filtro);
            }
            catch (Exception ex)
            {

            }
            
        }

        private void frmVentas_Load(object sender, EventArgs e)
        {
            
            try
            {
                obtenerUltimaVenta();//saber cual es el id de la ultima venta
                //buscarUltimaVenta((Convert.ToInt32(txtId.Text)).ToString());
                //buscarUltimaVenta("1"/*(Convert.ToInt32(txtId.Text)-1).ToString()*/); //llenar el grid con las ventas con el id de la ultima venta
            }
            catch (Exception ex)
            {
                //MessageBox.Show("no se pudo " + ex);
            }
        }

        private void txtProducto_Click(object sender, EventArgs e)
        {
            obtenerProducto("");            
        }



        public void limpiar()
        {
            lblCosto.Text = "0";
            lblCostoCompra.Text = "0";
            lblSubtotal.Text = "0";
            lblSubtotalCompra.Text = "0";
            txtProducto.Text = "";
            txtCantidad.Value =1;
        }


        private void guardarventa()
        {
            _ventasManejador.Guardar(new Ventas
            {
                Id = int.Parse(txtId.Text),
                Idp = Convert.ToInt32(txtId.Text),
                Producto = txtProducto.SelectedValue.ToString(),
                Fecha = Fecha.Text,
                Cantidad = Convert.ToInt32(txtCantidad.Value),
                Precio = Convert.ToDouble(lblCosto.Text),
                Total = Convert.ToDouble(lblSubtotal.Text),
            }, int.Parse(lblStatus.Text),"venta"); ;
        }
        private void btnAgrega_Click(object sender, EventArgs e)
        {
            lblTotal.Text = (Convert.ToInt32(lblTotal.Text) + Convert.ToInt32(lblSubtotal.Text)).ToString();
            lblTotalCompra.Text = (Convert.ToDouble(lblTotalCompra.Text) + Convert.ToInt32(lblSubtotalCompra.Text)).ToString();
            _inventarioManejador.Compra(txtProducto.Text, Convert.ToInt32(txtCantidad.Value));
            guardarventa();
            btnEliminar.Visible = true;
            btnCancelar.Enabled = false ;
            btnGuardar.Enabled = true;
            buscarUltimaVenta((Convert.ToInt32(txtId.Text)).ToString());
            limpiar();
        }
        private void Modificar()
        {
           // controlarcuadros(true);
            //controlarbotones(false, true, true, false);
            lblStatus.Text = dgvDatos.CurrentRow.Cells["id"].Value.ToString();
            lblCantidadE.Text = dgvDatos.CurrentRow.Cells["cantidad"].Value.ToString();
            lblTotalE.Text = dgvDatos.CurrentRow.Cells["total"].Value.ToString();
        }


        private void dgvDatos_Click(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = "1";
                Modificar();
                //buscarGastos("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void controlarbotones(bool a)
        {
            gbtexto.Visible=a;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            obtenerUltimaVenta();
            gbtexto.Visible = true;
            controlarbotones(true);
            buscarUltimaVenta((Convert.ToInt32(txtId.Text)).ToString());
            //btnCancelar.Enabled = true;
            //btnEliminar.Enabled = false;
            btnCancelar.Visible = true;
            btnCancelar.Enabled = true;
            btnEliminar.Visible = false;
            btnGuardar.Enabled = false;
            btnAgregar.Visible = false;
            btnEliminar.Visible = false;
        }

        private void eliminarP1()
        {
            
            var id = dgvDatos.CurrentRow.Cells["id"].Value;
            lblTotalE.Text = (dgvDatos.CurrentRow.Cells["total"].Value).ToString();
            lblCantidadE.Text = (dgvDatos.CurrentRow.Cells["cantidad"].Value).ToString();
            lblNombreProducto.Text= (dgvDatos.CurrentRow.Cells["producto"].Value).ToString();
            _inventarioManejador.CompraEliminar(lblNombreProducto.Text, Convert.ToInt32(lblCantidadE.Text));
            lblTotal.Text=(Convert.ToDouble(lblTotal.Text) - Convert.ToDouble(lblTotalE.Text)).ToString();
            string x = obtenerPrecioVentaCostoC(lblNombreProducto.Text,"");
            lblTotalCompra.Text = (Convert.ToDouble(lblTotalCompra.Text) - (Convert.ToInt32(x) * Convert.ToInt32(lblCantidadE.Text))).ToString();
        }

        private void eliminar()
        {
            eliminarP1();
            var id = dgvDatos.CurrentRow.Cells["id"].Value;
            _ventasManejador.Eliminar(Convert.ToInt32(id));
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el registro", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminar();
                    buscarUltimaVenta((Convert.ToInt32(txtId.Text) - 1).ToString());
                    buscarUltimaVenta((Convert.ToInt32(txtId.Text)).ToString());
                    lblStatus.Text = "0";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    lblStatus.Text = "0";
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            gbtexto.Visible = false;
            controlarbotones(false);
            controlarbotones(false);
            dgvDatos.Columns.Clear();
            btnEliminar.Visible = false;
            btnGuardar.Enabled=false;
            lblTotal.Text = "0";
            lblTotalCompra.Text = "0";
            btnAgregar.Visible = true;
        }
        private void guardarTotalVenta()
        {
            _ventatManejador.GuardarEnVenta(Convert.ToInt32(txtId.Text), Convert.ToDouble(lblTotalCompra.Text), Convert.ToDouble(lblTotal.Text), Fecha.Text);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (lblTotal.Text != "0")
            {
                controlarbotones(false);
                guardarTotalVenta();
                limpiar();
                dgvDatos.Columns.Clear();
                gbtexto.Visible = false;
                lblTotal.Text = "0";
                lblTotalCompra.Text = "0";
                btnAgregar.Visible = true;
                btnEliminar.Visible = false;
                //lblSubtotalCompra.Text = "0";
            }
            else 
            {
                btnAgregar.Visible = true;
                limpiar();
                dgvDatos.Columns.Clear();
                gbtexto.Visible = false;
            }
            
        }
        private string obtenerPrecioVentaCostoC(string filtro, string x)
        {
            if (txtProducto.SelectedValue.ToString() != "Entidades.Productos")
            {
                x=obtenerPrecioVentaC(filtro);
            }
            return x;
        }
        private void obtenerPrecioVentaCostoB(string filtro)
        {
            if (txtProducto.SelectedValue.ToString() != "Entidades.Productos")
            {
                obtenerPrecioVentaB(txtProducto.SelectedValue.ToString());
            }
        }
        private void txtProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSubtotal.Text = "0";
            lblSubtotalCompra.Text = "0";
            // lblSubtotal.Text = (txtCantidad.Value * Convert.ToInt32(lblCosto.Text)).ToString();
            obtenerPrecioVentaCosto(txtProducto.Text);
            obtenerPrecioVentaCostoB(txtProducto.Text);
            verificacion();
        }
        private void verificacion()
        {
            if (txtProducto.SelectedValue.ToString() != "Entidades.Productos")
            {
                try
                {
                    lblSubtotal.Text = (Convert.ToDouble(lblCosto.Text) * Convert.ToInt32(txtCantidad.Value.ToString())).ToString();
                    lblSubtotalCompra.Text = (Convert.ToDouble(lblCostoCompra.Text) * Convert.ToInt32(txtCantidad.Value.ToString())).ToString();
                }
                catch (Exception ex)
                {
                }  
            }
        }

        private void txtCantidad_ValueChanged(object sender, EventArgs e)
        {
            lblSubtotal.Text = (Convert.ToDouble(lblCosto.Text) * Convert.ToInt32(txtCantidad.Value)).ToString();
            lblSubtotalCompra.Text = (Convert.ToDouble(lblCostoCompra.Text) * Convert.ToInt32(txtCantidad.Value)).ToString();
        }

        private void lblSubtotal_Click(object sender, EventArgs e)
        {

        }
    }
}
